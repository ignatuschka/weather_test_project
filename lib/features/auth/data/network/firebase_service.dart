import 'package:firebase_auth/firebase_auth.dart';

import 'package:weather_test_project/features/auth/data/models/user_model.dart';

abstract class FirebaseService {
  Future<String?> signIn({required UserModel userModel});
  Future<void> signOut();
}

class FirebaseServiceImpl implements FirebaseService {
  final FirebaseAuth firebaseAuth;
  FirebaseServiceImpl({required this.firebaseAuth});

  @override
  Future<String?> signIn({required UserModel userModel}) async {
    final result = await firebaseAuth.signInWithEmailAndPassword(
      email: userModel.email ?? '',
      password: userModel.password ?? '',
    );
    return result.user?.uid;
  }

  @override
  Future<void> signOut() async => await FirebaseAuth.instance.signOut();
}
