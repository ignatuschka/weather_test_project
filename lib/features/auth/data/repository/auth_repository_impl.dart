import 'package:weather_test_project/features/auth/data/data_source/auth_remote_data_source.dart';
import 'package:weather_test_project/features/auth/domain/entity/user_entity.dart';
import 'package:weather_test_project/features/auth/domain/repository/auth_repository.dart';

class AuthRepositoryImpl implements AuthRepository {
  final AuthRemoteDataSource authRemoteDataSource;
  AuthRepositoryImpl({
    required this.authRemoteDataSource,
  });

  @override
  Future<String?> signIn({required UserEntity userEntity}) async =>
      await authRemoteDataSource.signIn(userModel: userEntity.toModel());

  @override
  Future<void> signOut() async => await authRemoteDataSource.signOut();
}
