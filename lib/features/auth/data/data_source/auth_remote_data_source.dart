import 'package:weather_test_project/core/base_usecase.dart';
import 'package:weather_test_project/features/auth/data/models/user_model.dart';
import 'package:weather_test_project/features/auth/data/network/firebase_service.dart';

abstract class AuthRemoteDataSource {
  Future<String?> signIn({required UserModel userModel});
  Future<void> signOut();
}

class AuthRemoteDataSourceImpl implements AuthRemoteDataSource {
  final FirebaseService firebaseService;
  AuthRemoteDataSourceImpl({
    required this.firebaseService,
  });

  @override
  Future<String?> signIn({required UserModel userModel}) async => await checkFirebaseErrors(() async => await firebaseService.signIn(userModel: userModel));

  @override
  Future<void> signOut() async => await checkFirebaseErrors(() async => await firebaseService.signOut());
}
