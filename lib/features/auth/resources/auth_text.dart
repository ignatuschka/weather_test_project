abstract class AuthText {
  static const String email = "Email";
  static const String pass = "Пароль";
  static const String loginButton = "Войти";
  static const String entance = "Вход";
  static const String enterSignInData = "Введите данные для входа";
  static const String enterPassword = "Введите пароль";
  static const String enterValidEmail = "Введите корректный email";
  static const String enterEmail = "Введите email";
  static const String invalidPasswordLenght = "Длина должна быть не менее 8 символов\n";
  static const String noSpecialCharacter = "Должен содержать специальный символ\n";
  static const String noDigit = "Должен содержать цифру\n";
  static const String noLowerCase = "Должен содержать строчную букву\n";
  static const String noUpperCase = "Должен содержать заглавную букву\n";
  static const String failedWith = "Процесс закончен с ошибкой:";
  static const String copy = "Скопировать";
  static const String somethingWrong = "Что-то произошло. Попробуйте позднее";
}