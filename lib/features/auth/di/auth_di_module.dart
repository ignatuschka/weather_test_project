import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:weather_test_project/features/auth/data/data_source/auth_remote_data_source.dart';
import 'package:weather_test_project/features/auth/data/network/firebase_service.dart';
import 'package:weather_test_project/features/auth/data/repository/auth_repository_impl.dart';
import 'package:weather_test_project/features/auth/domain/repository/auth_repository.dart';
import 'package:weather_test_project/features/auth/domain/usecase/sign_in_usecase.dart';
import 'package:weather_test_project/features/auth/domain/usecase/sign_out_usecase.dart';
import 'package:weather_test_project/features/auth/presentation/screens/sign_in_screen/sign_in_screen.dart';

import '../presentation/screens/sign_in_screen/bloc/sign_in_bloc.dart';

class AuthDIModule {
  void updateInjections(GetIt instance) {
    instance
        .registerLazySingleton<AuthRemoteDataSource>(() => AuthRemoteDataSourceImpl(firebaseService: instance.get()));

    instance.registerSingleton<FirebaseAuth>(FirebaseAuth.instance);

    instance.registerLazySingleton<FirebaseService>(() => FirebaseServiceImpl(firebaseAuth: instance.get()));

    instance.registerLazySingleton<AuthRepository>(() => AuthRepositoryImpl(authRemoteDataSource: instance.get()));

    instance.registerFactory(() => SignInUseCase(authRepository: instance.get()));

    instance.registerFactory(() => SignOutUseCase(authRepository: instance.get()));
  }
}

class SignInPage extends StatelessWidget {
  const SignInPage({super.key});

  GetIt get getIt => GetIt.I;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => SignInBloc(
        navigator: getIt(),
        signInUseCase: getIt(),
        instance: getIt(),
      ),
      child: const SignInScreen(),
    );
  }
}
