import 'package:weather_test_project/features/auth/domain/entity/user_entity.dart';

abstract class AuthRepository {
  Future<String?> signIn({required UserEntity userEntity});
  Future<void> signOut();
}