import 'package:weather_test_project/features/auth/data/models/user_model.dart';

class UserEntity {
  final String? email;
  final String? password;
  const UserEntity({
    required this.email,
    required this.password,
  });

  UserModel toModel() => UserModel(email: email, password: password);
}
