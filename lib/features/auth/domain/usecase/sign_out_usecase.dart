import 'package:weather_test_project/core/base_usecase.dart';
import 'package:weather_test_project/features/auth/domain/entity/user_entity.dart';
import 'package:weather_test_project/features/auth/domain/repository/auth_repository.dart';

class SignOutUseCase extends BaseUsecase<void, UserEntity> {
  final AuthRepository authRepository;
  SignOutUseCase({
    required this.authRepository,
  });

  @override
  Future<void> call(UserEntity params) async => await authRepository.signIn(userEntity: params);
}
