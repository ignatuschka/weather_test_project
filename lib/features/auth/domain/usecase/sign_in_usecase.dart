import 'package:weather_test_project/core/base_usecase.dart';
import 'package:weather_test_project/features/auth/domain/entity/user_entity.dart';
import 'package:weather_test_project/features/auth/domain/repository/auth_repository.dart';

class SignInUseCase extends BaseUsecase<String?, UserEntity> {
  final AuthRepository authRepository;
  SignInUseCase({
    required this.authRepository,
  });

  @override
  Future<String?> call(UserEntity params) async => await authRepository.signIn(userEntity: params);
}
