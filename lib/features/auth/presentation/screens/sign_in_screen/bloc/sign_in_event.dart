abstract class SignInEvent {
  const SignInEvent();
}

class SignInInitialEvent extends SignInEvent {}

class SignInTapEvent extends SignInEvent {
  final String email;
  final String password;
  const SignInTapEvent({
    required this.email,
    required this.password,
  });
}

class ShowPasswordEvent extends SignInEvent {}

class NavigateToMainEvent extends SignInEvent {}

class EmailValidationEvent extends SignInEvent {
  final String email;
  const EmailValidationEvent({
    required this.email,
  });
}

class PasswordValidationEvent extends SignInEvent {
  final String password;
  const PasswordValidationEvent({
    required this.password,
  });
}

class SignInExceptionEvent extends SignInEvent {}
