import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:weather_test_project/core/base_usecase.dart';
import 'package:weather_test_project/features/auth/domain/entity/user_entity.dart';
import 'package:weather_test_project/features/auth/domain/usecase/sign_in_usecase.dart';
import 'package:weather_test_project/features/auth/presentation/screens/sign_in_screen/bloc/sign_in_state.dart';
import 'package:weather_test_project/features/auth/resources/auth_text.dart';
import 'package:weather_test_project/navigation/auth_navigation.dart';

class SignInBloc extends Cubit<SignInState> {
  final AuthNavigator navigator;
  final SignInUseCase signInUseCase;
  final SharedPreferences instance;

  SignInBloc({
    required this.navigator,
    required this.signInUseCase,
    required this.instance,
  }) : super(SignInState.initial());


  Future<void> signInTapEvent({required String email, required String password}) async {
    emit(state.setIsLoadion(true));
    emailValidation(email: email);
    passwordValidation(password: password);
    if (state.isValidPassword && state.isValidEmail) {
      await processUseCase<String?>(
        () async => await signInUseCase(UserEntity(email: email, password: password)),
        onSucess: (result) async {
          if (result != null) instance.setString('uid', result);
          navigator.navigateToMainScreen();
        },
      );
    }
    emit(state
        .setIsLoadion(false)
        .setEnteredInvalidPassword(!state.isValidPassword)
        .setEnteredInvalidEmail(!state.isValidEmail));
  }

  void showPass() {
    emit(state.setShowPass(!state.showPass));
  }

  void emailValidation({required String email}) {
    String errorText = '';
    if (email.isEmpty) {
      errorText = AuthText.enterEmail;
    } else {
      if (!RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(email)) {
        errorText = AuthText.enterValidEmail;
      }
    }
    emit(state.setErrorEmailValidationText(errorText).setIsValidEmail(errorText.isEmpty));
  }

  void passwordValidation({required String password}) {
    String errorText = '';
    if (password.isEmpty) {
      errorText = AuthText.enterPassword;
    } else {
      if (!RegExp(r'^(?=.*?[A-Z]).{1,}$').hasMatch(password)) errorText += AuthText.noUpperCase;
      if (!RegExp(r'^(?=.*?[a-z]).{1,}$').hasMatch(password)) errorText += AuthText.noLowerCase;
      if (!RegExp(r'^(?=.*?[0-9]).{1,}$').hasMatch(password)) errorText += AuthText.noDigit;
      if (!RegExp(r'^(?=.*?[!@#\$&*~]).{1,}$').hasMatch(password)) errorText += AuthText.noSpecialCharacter;
      if (!RegExp(r'^.{8,}$').hasMatch(password)) errorText += AuthText.invalidPasswordLenght;
    }
    emit(state.setErrorPasswordValidationText(errorText).setIsValidPassword(errorText.isEmpty));
  }
}
