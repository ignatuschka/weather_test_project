import 'package:built_value/built_value.dart';

part 'sign_in_state.g.dart';

abstract class SignInState implements Built<SignInState, SignInStateBuilder> {
  bool get isLoading;
  bool get showPass;
  String get email;
  String get password;
  bool get isLoginDataCorrect;
  bool get isValidEmail;
  bool get isValidPassword;
  String get errorPasswordValidationText;
  String get errorEmailValidationText;
  bool get enteredInvalidPassword;
  bool get enteredInvalidEmail;
  String get exception;
  bool get showSnackBar;
  

  SignInState._();

  factory SignInState([void Function(SignInStateBuilder)? updates]) = _$SignInState;

  factory SignInState.initial() => SignInState((b) => b
    ..isLoading = false
    ..showPass = false
    ..email = ''
    ..password = ''
    ..isLoginDataCorrect = false
    ..isValidEmail = false
    ..isValidPassword = false
    ..errorPasswordValidationText = ''
    ..errorEmailValidationText = ''
    ..enteredInvalidPassword = false
    ..enteredInvalidEmail = false
    ..exception = ''
    ..showSnackBar = false);

    

  SignInState setIsLoadion(bool isLoading) => rebuild((b) => (b)..isLoading = isLoading);

  SignInState setShowPass(bool showPass) => rebuild((b) => (b)..showPass = showPass);

  SignInState setEmail(String email) => rebuild((b) => (b)..email = email);

  SignInState setPassword(String password) => rebuild((b) => (b)..password = password);

  SignInState setIsLoginDataCorrect(bool isLoginDataCorrect) =>
      rebuild((b) => (b)..isLoginDataCorrect = isLoginDataCorrect);

  SignInState setIsValidEmail(bool isValidEmail) => rebuild((b) => (b)..isValidEmail = isValidEmail);

  SignInState setIsValidPassword(bool isValidPassword) => rebuild((b) => (b)..isValidPassword = isValidPassword);

  SignInState setErrorPasswordValidationText(String errorPasswordValidationText) =>
      rebuild((b) => (b)..errorPasswordValidationText = errorPasswordValidationText);

  SignInState setErrorEmailValidationText(String errorEmailValidationText) =>
      rebuild((b) => (b)..errorEmailValidationText = errorEmailValidationText);

  SignInState setEnteredInvalidPassword(bool enteredInvalidPassword) =>
      rebuild((b) => (b)..enteredInvalidPassword = enteredInvalidPassword);

  SignInState setEnteredInvalidEmail(bool enteredInvalidEmail) =>
      rebuild((b) => (b)..enteredInvalidEmail = enteredInvalidEmail);

  SignInState setException(String exception) => rebuild((b) => (b)..exception = exception);

  SignInState setShowSnackBar(bool showSnackBar) =>
      rebuild((b) => (b)..showSnackBar = showSnackBar);
}
