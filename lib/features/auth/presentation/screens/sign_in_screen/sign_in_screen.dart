import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_test_project/core/custom_fonts.dart';
import 'package:weather_test_project/core/theme/app_color_theme.dart';
import 'package:weather_test_project/features/auth/presentation/screens/sign_in_screen/bloc/sign_in_bloc.dart';
import 'package:weather_test_project/features/auth/presentation/screens/sign_in_screen/bloc/sign_in_state.dart';
import 'package:weather_test_project/features/auth/presentation/widgets/body_medium.dart';
import 'package:weather_test_project/features/auth/presentation/widgets/custom_button.dart';
import 'package:weather_test_project/features/auth/presentation/widgets/custom_textfield.dart';
import 'package:weather_test_project/features/auth/presentation/widgets/heading_large.dart';
import 'package:weather_test_project/features/auth/resources/auth_text.dart';

class SignInScreen extends StatefulWidget {
  const SignInScreen({super.key});

  @override
  State<SignInScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<SignInScreen> {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SignInBloc, SignInState>(
      builder: (context, state) {
        return Scaffold(
          resizeToAvoidBottomInset: false,
          body: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Padding(
                    padding: EdgeInsets.only(top: 68, bottom: 12),
                    child: HeadingLarge(text: AuthText.entance),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 16),
                    child: BodyMedium(
                      text: AuthText.enterSignInData,
                      color: const LightColorTheme().greyMedium,
                    ),
                  ),
                  CustomTextField(
                    controller: emailController,
                    onChanged: (email) {
                      if (state.enteredInvalidEmail) {
                        context.read<SignInBloc>().emailValidation(email: email);
                      }
                    },
                    errorText:
                        (state.enteredInvalidEmail && !state.isValidEmail) ? state.errorEmailValidationText : null,
                    hintText: AuthText.email,
                  ),
                  CustomTextField(
                    obscureText: !state.showPass,
                    controller: passwordController,
                    errorText: (state.enteredInvalidPassword && !state.isValidPassword)
                        ? state.errorPasswordValidationText
                        : null,
                    hintText: AuthText.pass,
                    onSuffixIconPressed: () {
                      context.read<SignInBloc>().showPass();
                    },
                    suffixIcon: state.showPass ? CustomIcons.eyeOff2 : CustomIcons.eye,
                    onChanged: (password) {
                      if (state.enteredInvalidPassword) {
                        context.read<SignInBloc>().passwordValidation(password: password);
                      }
                    },
                  ),
                  CustomButton(
                    onTap: () async {
                      if (!state.isLoading) {
                        context.read<SignInBloc>().signInTapEvent(
                              email: emailController.text,
                              password: passwordController.text,
                            );
                      }
                    },
                    text: AuthText.loginButton,
                    state: state.isLoading,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
