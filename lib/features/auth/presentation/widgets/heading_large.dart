import 'package:flutter/material.dart';
import 'package:weather_test_project/core/theme/app_color_theme.dart';

class HeadingLarge extends StatelessWidget {
  final String text;
  final Color? color;

  const HeadingLarge({
    Key? key,
    required this.text,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        fontFamily: 'Ubuntu',
        fontWeight: FontWeight.w500,
        fontStyle: FontStyle.normal,
        fontSize: 32,
        height: 36 / 32,
        color: color ?? const LightColorTheme().black,
        overflow: TextOverflow.fade,
      ),
    );
  }
}
