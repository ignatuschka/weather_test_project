import 'package:flutter/material.dart';
import 'package:weather_test_project/core/theme/app_color_theme.dart';

class CustomLoadingIndicator extends StatelessWidget {
  final double strokeWidth;
  final Color? color;

  const CustomLoadingIndicator({
    Key? key,
    this.strokeWidth = 4.0,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          CircularProgressIndicator(
            color: color ?? const LightColorTheme().white,
            strokeWidth: strokeWidth,
          ),
        ],
      ),
    );
  }
}
