import 'package:flutter/material.dart';
import 'package:weather_test_project/core/theme/app_color_theme.dart';

class HeadingMedium extends StatelessWidget {
  final String text;
  final Color? color;
  final TextAlign? textAlign;

  const HeadingMedium({
    Key? key,
    required this.text,
    this.color,
    this.textAlign,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: textAlign,
      style: TextStyle(
        fontFamily: 'Ubuntu',
        fontWeight: FontWeight.w500,
        fontStyle: FontStyle.normal,
        fontSize: 22,
        height: 28 / 22,
        color: color ?? const LightColorTheme().black,
        overflow: TextOverflow.fade,
      ),
    );
  }
}
