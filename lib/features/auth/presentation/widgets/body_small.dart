import 'package:flutter/material.dart';
import 'package:weather_test_project/core/theme/app_color_theme.dart';

class BodySmall extends StatelessWidget {
  final String text;
  final Color? color;

  const BodySmall({
    Key? key,
    required this.text,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        fontFamily: 'Roboto',
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
        fontSize: 13,
        height: 18 / 13,
        color: color ?? const LightColorTheme().black,
        overflow: TextOverflow.fade,
      ),
    );
  }
}
