import 'package:flutter/material.dart';
import 'package:weather_test_project/core/theme/app_color_theme.dart';

class BodyMediumBold extends StatelessWidget {
  final String text;
  final Color? color;

  const BodyMediumBold({
    Key? key,
    required this.text,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        fontFamily: 'Roboto',
        fontWeight: FontWeight.w500,
        fontStyle: FontStyle.normal,
        fontSize: 15,
        height: 22 / 15,
        color: color ?? const LightColorTheme().black,
        overflow: TextOverflow.fade,
      ),
    );
  }
}
