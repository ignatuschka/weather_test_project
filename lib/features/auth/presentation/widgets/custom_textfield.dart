import 'package:flutter/material.dart';
import 'package:weather_test_project/core/theme/app_color_theme.dart';
import 'package:weather_test_project/features/auth/presentation/widgets/body_large.dart';

class CustomTextField extends StatelessWidget {
  final TextEditingController? controller;
  final String? errorText;
  final String hintText;
  final bool obscureText;
  final IconData? suffixIcon;
  final void Function(String)? onChanged;
  final void Function()? onSuffixIconPressed;
  const CustomTextField({
    Key? key,
    this.controller,
    this.errorText,
    required this.hintText,
    this.suffixIcon,
    this.obscureText = false,
    this.onChanged,
    this.onSuffixIconPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8, bottom: 24),
      child: TextField(
        onTapOutside: (event) => FocusScope.of(context).unfocus(),
        controller: controller,
        decoration: InputDecoration(
          alignLabelWithHint: true,
          enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: const LightColorTheme().greyBorder)),
          focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: const LightColorTheme().blue)),
          label: BodyLarge(
            text: hintText,
            color: const LightColorTheme().greyMedium,
          ),
          errorText: errorText,
          suffixIcon: IconButton(
            onPressed: onSuffixIconPressed,
            icon: Icon(suffixIcon, color: const LightColorTheme().blue),
          ),
        ),
        obscureText: obscureText,
        onChanged: onChanged,
      ),
    );
  }
}
