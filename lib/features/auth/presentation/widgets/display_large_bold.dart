import 'package:flutter/material.dart';
import 'package:weather_test_project/core/theme/app_color_theme.dart';

class DisplayLargeBold extends StatelessWidget {
  final String text;
  final Color? color;

  const DisplayLargeBold({
    Key? key,
    required this.text,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        fontFamily: 'Ubuntu',
        fontWeight: FontWeight.w500,
        fontStyle: FontStyle.normal,
        fontSize: 64,
        height: 72 / 64,
        color: color ?? const LightColorTheme().black,
        overflow: TextOverflow.fade,
      ),
    );
  }
}
