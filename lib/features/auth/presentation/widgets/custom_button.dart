import 'package:flutter/material.dart';
import 'package:weather_test_project/core/theme/app_color_theme.dart';
import 'package:weather_test_project/features/auth/presentation/widgets/body_large_bold.dart';
import 'package:weather_test_project/features/auth/presentation/widgets/custom_loading_indicator.dart';

class CustomButton extends StatelessWidget {
  final void Function() onTap;
  final String text;
  final bool state;
  final double height;
  final double width;
  const CustomButton({
    Key? key,
    required this.onTap,
    required this.text,
    this.height = 48,
    this.width = double.infinity,
    this.state = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 24),
      child: InkWell(
        onTap: onTap,
        borderRadius: BorderRadius.circular(24),
        child: Ink(
          decoration: BoxDecoration(
            color: const LightColorTheme().blue,
            borderRadius: BorderRadius.circular(24),
          ),
          height: height,
          width: width,
          child: Center(
            child: state
                ? const CustomLoadingIndicator()
                : BodyLargeBold(
                    text: text,
                    color: const LightColorTheme().white,
                  ),
          ),
        ),
      ),
    );
  }
}
