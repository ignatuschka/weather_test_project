import 'package:location/location.dart';
import 'package:weather_test_project/features/main/data/data_source/main_local_data_source.dart';

import 'package:weather_test_project/features/main/data/data_source/main_remote_data_source.dart';
import 'package:weather_test_project/features/main/domain/entity/weather_entity.dart';
import 'package:weather_test_project/features/main/domain/repository/main_repository.dart';

class MainRepositoryImpl implements MainRepository {
  final MainRemoteDataSource remoteDataSource;
  final MainLocalDataSource localDataSource;
  MainRepositoryImpl({
    required this.remoteDataSource,
    required this.localDataSource,
  });

  @override
  Future<LocationData> getCurrentLocation() async {
    return await remoteDataSource.getCurrentLocation();
  }

  @override
  Future<WeatherEntity> getWeather({required String lonLat}) async {
    final result = await remoteDataSource.getWeather(lonLat: lonLat);
    return result.toEntity();
  }

  @override
  Future<bool> requestPermission() async => await remoteDataSource.requestPermission();
  
  
  @override
  Future<void> createOrUpdateWeather({required WeatherEntity model}) async =>
    await localDataSource.createOrUpdateWeather(model: model.toModel());
  
  
  @override
  Future<WeatherEntity> getWeatherForecast() async {
    final result = await localDataSource.getWeatherForecast();
    return result.toEntity();
  }
  
  @override
  Future<bool> checkConnection() async => await remoteDataSource.checkConnection();
  
  @override
  Future<void> openAppSettings() async => await remoteDataSource.openAppSettings();
  
  @override
  Future<void> openLocationSettings() async => await remoteDataSource.openLocationSettings();
}
