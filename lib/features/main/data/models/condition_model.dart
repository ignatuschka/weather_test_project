import 'package:json_annotation/json_annotation.dart';
import 'package:weather_test_project/features/main/domain/entity/condition_entity.dart';

part 'condition_model.g.dart';

@JsonSerializable()
class ConditionModel {
  String? text;
  String? icon;
  int? code;
  ConditionModel({
    required this.text,
    required this.icon,
    required this.code,
  });

  factory ConditionModel.fromJson(Map<String, dynamic> json) => _$ConditionModelFromJson(json);

  Map<String, dynamic> toJson() => _$ConditionModelToJson(this);

  ConditionEntity toEntity() => ConditionEntity(
    text: text,
    icon: icon,
    code: code,
  );
}
