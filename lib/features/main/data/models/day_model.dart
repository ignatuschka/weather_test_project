import 'package:json_annotation/json_annotation.dart';
import 'package:weather_test_project/features/main/data/models/condition_model.dart';
import 'package:weather_test_project/features/main/domain/entity/day_entity.dart';

part 'day_model.g.dart';

@JsonSerializable()
class DayModel {
  @JsonKey(name: 'maxtemp_c')
  double? maxtempC;
  @JsonKey(name: 'maxtemp_f')
  double? maxtempF;
  @JsonKey(name: 'mintemp_c')
  double? mintempC;
  @JsonKey(name: 'mintemp_f')
  double? mintempF;
  @JsonKey(name: 'avgtemp_c')
  double? avgtempC;
  @JsonKey(name: 'avgtemp_f')
  double? avgtempF;
  @JsonKey(name: 'maxwind_mph')
  double? maxwindMph;
  @JsonKey(name: 'maxwind_kph')
  double? maxwindKph;
  @JsonKey(name: 'totalprecip_mm')
  double? totalprecipMm;
  @JsonKey(name: 'totalprecip_in')
  double? totalprecipIn;
  @JsonKey(name: 'totalsnow_cm')
  double? totalsnowCm;
  @JsonKey(name: 'avgvis_km')
  double? avgvisKm;
  @JsonKey(name: 'avgvis_miles')
  double? avgvisMiles;
  double? avghumidity;
  @JsonKey(name: 'daily_will_it_rain')
  int? dailyWillItRain;
  @JsonKey(name: 'daily_chance_of_rain')
  double? dailyChanceOfRain;
  @JsonKey(name: 'daily_will_it_snow')
  int? dailyWillItSnow;
  @JsonKey(name: 'daily_chance_of_snow')
  double? dailyChanceOfSnow;
  ConditionModel? condition;
  double? uv;
  DayModel({
    required this.maxtempC,
    required this.maxtempF,
    required this.mintempC,
    required this.mintempF,
    required this.avgtempC,
    required this.avgtempF,
    required this.maxwindMph,
    required this.maxwindKph,
    required this.totalprecipMm,
    required this.totalprecipIn,
    required this.totalsnowCm,
    required this.avgvisKm,
    required this.avgvisMiles,
    required this.avghumidity,
    required this.dailyWillItRain,
    required this.dailyChanceOfRain,
    required this.dailyWillItSnow,
    required this.dailyChanceOfSnow,
    required this.condition,
    required this.uv,
  });

  factory DayModel.fromJson(Map<String, dynamic> json) => _$DayModelFromJson(json);

  Map<String, dynamic> toJson() => _$DayModelToJson(this);

  DayEntity toEntity() => DayEntity(
    maxtempC: maxtempC,
    maxtempF: maxtempF,
    mintempC: mintempC,
    mintempF: mintempF,
    avgtempC: avgtempC,
    avgtempF: avgtempF,
    maxwindMph: maxwindMph,
    maxwindKph: maxwindKph,
    totalprecipMm: totalprecipMm,
    totalprecipIn: totalprecipIn,
    totalsnowCm: totalsnowCm,
    avgvisKm: avgvisKm,
    avgvisMiles: avgvisMiles,
    avghumidity: avghumidity,
    dailyWillItRain: dailyWillItRain,
    dailyChanceOfRain: dailyChanceOfRain,
    dailyWillItSnow: dailyWillItSnow,
    dailyChanceOfSnow: dailyChanceOfSnow,
    condition: condition?.toEntity(),
    uv: uv,
  );
}
