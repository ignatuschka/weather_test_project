import 'package:json_annotation/json_annotation.dart';
import 'package:weather_test_project/features/main/data/models/astro_model.dart';
import 'package:weather_test_project/features/main/data/models/day_model.dart';
import 'package:weather_test_project/features/main/data/models/hour_model.dart';
import 'package:weather_test_project/features/main/domain/entity/forecast_day_entity.dart';

part 'forecast_day_model.g.dart';

@JsonSerializable()
class ForecastDayModel {
  String? date;
  @JsonKey(name: 'date_epoch')
  int? dateEpoch;
  DayModel? day;
  AstroModel? astro;
  List<HourModel>? hour;
  ForecastDayModel({
    required this.date,
    required this.dateEpoch,
    required this.day,
    required this.astro,
    required this.hour,
  });

  factory ForecastDayModel.fromJson(Map<String, dynamic> json) => _$ForecastDayModelFromJson(json);

  Map<String, dynamic> toJson() => _$ForecastDayModelToJson(this);

  ForecastDayEntity toEntity() => ForecastDayEntity(
    date: date,
    dateEpoch: dateEpoch,
    day: day?.toEntity(),
    astro: astro?.toEntity(),
    hour: hour?.map((e) => e.toEntity()).toList(),
  );
}
