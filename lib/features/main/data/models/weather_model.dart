import 'package:json_annotation/json_annotation.dart';
import 'package:weather_test_project/features/main/data/models/current_model.dart';
import 'package:weather_test_project/features/main/data/models/forecast_model.dart';
import 'package:weather_test_project/features/main/data/models/location_model.dart';
import 'package:weather_test_project/features/main/domain/entity/weather_entity.dart';

part 'weather_model.g.dart';

@JsonSerializable()
class WeatherModel {
  LocationModel? location;
  CurrentModel? current;
  ForecastModel? forecast;
  WeatherModel({
    required this.location,
    required this.current,
    required this.forecast,
  });

  factory WeatherModel.fromJson(Map<String, dynamic> json) => _$WeatherModelFromJson(json);

  Map<String, dynamic> toJson() => _$WeatherModelToJson(this);

  WeatherEntity toEntity() => WeatherEntity(
    location: location?.toEntity(),
    current: current?.toEntity(),
    forecast: forecast?.toEntity(),
  );
}
