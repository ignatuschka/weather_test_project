import 'package:json_annotation/json_annotation.dart';
import 'package:weather_test_project/features/main/data/models/condition_model.dart';
import 'package:weather_test_project/features/main/domain/entity/current_entity.dart';

part 'current_model.g.dart';

@JsonSerializable()
class CurrentModel {
  @JsonKey(name: 'last_updated_epoch')
  int? lastUpdatedEpoch;
  @JsonKey(name: 'last_updated')
  String? lastUpdated;
  @JsonKey(name: 'temp_c')
  double? tempC;
  @JsonKey(name: 'temp_f')
  double? tempF;
  @JsonKey(name: 'is_day')
  int? isDay;
  ConditionModel? condition;
  @JsonKey(name: 'wind_mph')
  double? windMph;
  @JsonKey(name: 'wind_kph')
  double? windKph;
  @JsonKey(name: 'wind_degree')
  double? windDegree;
  @JsonKey(name: 'wind_dir')
  String? windDir;
  @JsonKey(name: 'pressure_mb')
  double? pressureMb;
  @JsonKey(name: 'pressure_in')
  double? pressureIn;
  @JsonKey(name: 'precip_mm')
  double? precipMm;
  @JsonKey(name: 'precip_in')
  double? precipIn;
  double? humidity;
  double? cloud;
  @JsonKey(name: 'feelslike_c')
  double? feelslikeC;
  @JsonKey(name: 'feelslike_f')
  double? feelslikeF;
  @JsonKey(name: 'vis_km')
  double? visKm;
  @JsonKey(name: 'vis_miles')
  double? visMiles;
  double? uv;
  @JsonKey(name: 'gust_mph')
  double? gustMph;
  @JsonKey(name: 'gust_kph')
  double? gustKph;
  CurrentModel({
    required this.lastUpdatedEpoch,
    required this.lastUpdated,
    required this.tempC,
    required this.tempF,
    required this.isDay,
    required this.condition,
    required this.windMph,
    required this.windKph,
    required this.windDegree,
    required this.windDir,
    required this.pressureMb,
    required this.pressureIn,
    required this.precipMm,
    required this.precipIn,
    required this.humidity,
    required this.cloud,
    required this.feelslikeC,
    required this.feelslikeF,
    required this.visKm,
    required this.visMiles,
    required this.uv,
    required this.gustMph,
    required this.gustKph,
  });

  factory CurrentModel.fromJson(Map<String, dynamic> json) => _$CurrentModelFromJson(json);

  Map<String, dynamic> toJson() => _$CurrentModelToJson(this);

  CurrentEntity toEntity() => CurrentEntity(
    lastUpdatedEpoch: lastUpdatedEpoch,
    lastUpdated: lastUpdated,
    tempC: tempC,
    tempF: tempF,
    isDay: isDay,
    condition: condition?.toEntity(),
    windMph: windMph,
    windKph: windKph,
    windDegree: windDegree,
    windDir: windDir,
    pressureMb: pressureMb,
    pressureIn: pressureIn,
    precipMm: precipMm,
    precipIn: precipIn,
    humidity: humidity,
    cloud: cloud,
    feelslikeC: feelslikeC,
    feelslikeF: feelslikeF,
    visKm: visKm,
    visMiles: visMiles,
    uv: uv,
    gustMph: gustMph,
    gustKph: gustKph,
  );
}
