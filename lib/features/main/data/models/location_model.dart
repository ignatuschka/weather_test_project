import 'package:json_annotation/json_annotation.dart';
import 'package:weather_test_project/features/main/domain/entity/location_entity.dart';

part 'location_model.g.dart';

@JsonSerializable()
class LocationModel {
  String? name;
  String? region;
  String? country;
  double? lat;
  double? lon;
  @JsonKey(name: 'tz_id')
  String? tzId;
  @JsonKey(name: 'localtime_epoch')
  int? localtimeEpoch;
  String? localtime;
  LocationModel({
    required this.name,
    required this.region,
    required this.country,
    required this.lat,
    required this.lon,
    required this.tzId,
    required this.localtimeEpoch,
    required this.localtime,
  });

  factory LocationModel.fromJson(Map<String, dynamic> json) => _$LocationModelFromJson(json);

  Map<String, dynamic> toJson() => _$LocationModelToJson(this);

  LocationEntity toEntity() => LocationEntity(
    name: name,
    region: region,
    country: country,
    lat: lat,
    lon: lon,
    tzId: tzId,
    localtimeEpoch: localtimeEpoch,
    localtime: localtime,
  );
}
