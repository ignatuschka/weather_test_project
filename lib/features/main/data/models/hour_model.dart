import 'package:json_annotation/json_annotation.dart';
import 'package:weather_test_project/features/main/data/models/condition_model.dart';
import 'package:weather_test_project/features/main/domain/entity/hour_entity.dart';

part 'hour_model.g.dart';

@JsonSerializable()
class HourModel {
  @JsonKey(name: 'time_epoch')
  int? timeEpoch;
  String? time;
  @JsonKey(name: 'temp_c')
  double? tempC;
  @JsonKey(name: 'temp_f')
  double? tempF;
  @JsonKey(name: 'is_day')
  int? isDay;
  ConditionModel? condition;
  @JsonKey(name: 'wind_mph')
  double? windMph;
  @JsonKey(name: 'wind_kph')
  double? windKph;
  @JsonKey(name: 'wind_degree')
  double? windDegree;
  @JsonKey(name: 'wind_dir')
  String? windDir;
  @JsonKey(name: 'pressure_mb')
  double? pressureMb;
  @JsonKey(name: 'pressure_in')
  double? pressureIn;
  @JsonKey(name: 'precip_mm')
  double? precipMm;
  @JsonKey(name: 'precip_in')
  double? precipIn;
  @JsonKey(name: 'snow_cm')
  double? snowCm;
  double? humidity;
  double? cloud;
  @JsonKey(name: 'feelslike_c')
  double? feelslikeC;
  @JsonKey(name: 'feelslike_f')
  double? feelslikeF;
  @JsonKey(name: 'windchill_c')
  double? windchillC;
  @JsonKey(name: 'windchill_f')
  double? windchillF;
  @JsonKey(name: 'heatindex_c')
  double? heatindexC;
  @JsonKey(name: 'heatindex_f')
  double? heatindexF;
  @JsonKey(name: 'dewpoint_c')
  double? dewpointC;
  @JsonKey(name: 'dewpoint_f')
  double? dewpointF;
  @JsonKey(name: 'will_it_rain')
  int? willItRain;
  @JsonKey(name: 'chance_of_rain')
  double? chanceOfRain;
  @JsonKey(name: 'will_it_snow')
  int? willItSnow;
  @JsonKey(name: 'chance_of_snow')
  double? chanceOfSnow;
  @JsonKey(name: 'vis_km')
  double? visKm;
  @JsonKey(name: 'vis_miles')
  double? visMiles;
  @JsonKey(name: 'gust_mph')
  double? gustMph;
  @JsonKey(name: 'gust_kph')
  double? gustKph;
  double? uv;
  HourModel({
    required this.timeEpoch,
    required this.time,
    required this.tempC,
    required this.tempF,
    required this.isDay,
    required this.condition,
    required this.windMph,
    required this.windKph,
    required this.windDegree,
    required this.windDir,
    required this.pressureMb,
    required this.pressureIn,
    required this.precipMm,
    required this.precipIn,
    required this.snowCm,
    required this.humidity,
    required this.cloud,
    required this.feelslikeC,
    required this.feelslikeF,
    required this.windchillC,
    required this.windchillF,
    required this.heatindexC,
    required this.heatindexF,
    required this.dewpointC,
    required this.dewpointF,
    required this.willItRain,
    required this.chanceOfRain,
    required this.willItSnow,
    required this.chanceOfSnow,
    required this.visKm,
    required this.visMiles,
    required this.gustMph,
    required this.gustKph,
    required this.uv,
  });

  factory HourModel.fromJson(Map<String, dynamic> json) => _$HourModelFromJson(json);

  Map<String, dynamic> toJson() => _$HourModelToJson(this);

  HourEntity toEntity() => HourEntity(
    timeEpoch: timeEpoch,
    time: time,
    tempC: tempC,
    tempF: tempF,
    isDay: isDay,
    condition: condition?.toEntity(),
    windMph: windMph,
    windKph: windKph,
    windDegree: windDegree,
    windDir: windDir,
    pressureMb: pressureMb,
    pressureIn: pressureIn,
    precipMm: precipMm,
    precipIn: precipIn,
    snowCm: snowCm,
    humidity: humidity,
    cloud: cloud,
    feelslikeC: feelslikeC,
    feelslikeF: feelslikeF,
    windchillC: windchillC,
    windchillF: windchillF,
    heatindexC: heatindexC,
    heatindexF: heatindexF,
    dewpointC: dewpointC,
    dewpointF: dewpointF,
    willItRain: willItRain,
    chanceOfRain: chanceOfRain,
    willItSnow: willItSnow,
    chanceOfSnow: chanceOfSnow,
    visKm: visKm,
    visMiles: visMiles,
    gustMph: gustMph,
    gustKph: gustKph,
    uv: uv,
  );
}
