import 'package:json_annotation/json_annotation.dart';
import 'package:weather_test_project/features/main/domain/entity/astro_entity.dart';

part 'astro_model.g.dart';

@JsonSerializable()
class AstroModel {
  String? sunrise;
  String? sunset;
  String? moonrise;
  String? moonset;
  @JsonKey(name: 'moon_phase')
  String? moonPhase;
  @JsonKey(name: 'moon_illumination')
  int? moonIllumination;
  @JsonKey(name: 'is_moon_up')
  int? isMoonUp;
  @JsonKey(name: 'is_sun_up')
  int? isSunUp;
  AstroModel({
    required this.sunrise,
    required this.sunset,
    required this.moonrise,
    required this.moonset,
    required this.moonPhase,
    required this.moonIllumination,
    required this.isMoonUp,
    required this.isSunUp,
  });

  factory AstroModel.fromJson(Map<String, dynamic> json) => _$AstroModelFromJson(json);

  Map<String, dynamic> toJson() => _$AstroModelToJson(this);

  AstroEntity toEntity() => AstroEntity(
    sunrise: sunrise,
    sunset: sunset,
    moonrise: moonrise,
    moonset: moonset,
    moonPhase: moonPhase,
    moonIllumination: moonIllumination,
    isMoonUp: isMoonUp,
    isSunUp: isSunUp,
  );
}
