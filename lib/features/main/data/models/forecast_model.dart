import 'package:json_annotation/json_annotation.dart';
import 'package:weather_test_project/features/main/data/models/forecast_day_model.dart';
import 'package:weather_test_project/features/main/domain/entity/forecast_entity.dart';

part 'forecast_model.g.dart';

@JsonSerializable()
class ForecastModel {
  List<ForecastDayModel>? forecastday;
  ForecastModel({
    required this.forecastday
  });

  factory ForecastModel.fromJson(Map<String, dynamic> json) => _$ForecastModelFromJson(json);

  Map<String, dynamic> toJson() => _$ForecastModelToJson(this);

  ForecastEntity toEntity() => ForecastEntity(
    forecastday: forecastday?.map((e) => e.toEntity()).toList(),
  );
}
