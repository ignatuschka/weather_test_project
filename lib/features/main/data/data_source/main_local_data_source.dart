import 'package:weather_test_project/features/main/data/database/daos/weather_dao.dart';
import 'package:weather_test_project/features/main/data/models/weather_model.dart';

abstract class MainLocalDataSource {
  Future<void> createOrUpdateWeather({required WeatherModel model});
  Future<WeatherModel> getWeatherForecast();
}

class MainLocalDataSourceImpl implements MainLocalDataSource {
  final WeatherDao weatherDao;
  MainLocalDataSourceImpl({required this.weatherDao});

  @override
  Future<void> createOrUpdateWeather({required WeatherModel model}) async =>
      await weatherDao.createOrUpdateWeather(model: model);

  @override
  Future<WeatherModel> getWeatherForecast() async => await weatherDao.getWeatherForecast();
}
