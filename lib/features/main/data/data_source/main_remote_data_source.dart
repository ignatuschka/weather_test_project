import 'package:location/location.dart';
import 'package:weather_test_project/features/main/data/models/weather_model.dart';
import 'package:weather_test_project/features/main/data/network/connection_service.dart';
import 'package:weather_test_project/features/main/data/network/location_service.dart';
import 'package:weather_test_project/features/main/data/network/main_api.dart';

abstract class MainRemoteDataSource {
  Future<WeatherModel> getWeather({required String lonLat});
  Future<bool> requestPermission();
  Future<LocationData> getCurrentLocation();
  Future<bool> checkConnection();
  Future<void> openAppSettings();
  Future<void> openLocationSettings();
}

class MainRemoteDataSourceImpl implements MainRemoteDataSource {
  final LocationService locationService;
  final MainApi api;
  final ConnectionService connectionService;
  MainRemoteDataSourceImpl({
    required this.locationService,
    required this.api,
    required this.connectionService,
  });

  @override
  Future<LocationData> getCurrentLocation() async => await locationService.getCurrentLocation();

  @override
  Future<WeatherModel> getWeather({required String lonLat}) async =>
      await api.getWeather(lonLat);

  @override
  Future<bool> requestPermission() async => await locationService.requestPermission();
  
  @override
  Future<bool> checkConnection() async => await connectionService.checkConnection();
  
  @override
  Future<void> openAppSettings() async => await locationService.openAppSettings();
  
  @override
  Future<void> openLocationSettings() async => await locationService.openLocationSettings();
}
