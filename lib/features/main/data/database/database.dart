import 'dart:io';

import 'package:drift/drift.dart';
import 'package:drift/native.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as p;
import 'package:weather_test_project/features/main/data/database/daos/weather_dao.dart';
import 'package:weather_test_project/features/main/data/database/tables/astro_table.dart';
import 'package:weather_test_project/features/main/data/database/tables/condition_table.dart';
import 'package:weather_test_project/features/main/data/database/tables/current_table.dart';
import 'package:weather_test_project/features/main/data/database/tables/day_table.dart';
import 'package:weather_test_project/features/main/data/database/tables/forecast_day_table.dart';
import 'package:weather_test_project/features/main/data/database/tables/hour_table.dart';
import 'package:weather_test_project/features/main/data/database/tables/location_table.dart';

part 'database.g.dart';

@DriftDatabase(
  daos: [WeatherDao],
  tables: [LocationTable, HourTable, ForecastdayTable, DayTable, CurrentTable, ConditionTable, AstroTable],
)
class AppDb extends _$AppDb {
  AppDb() : super(_openConnection());

  @override
  int get schemaVersion => 1;
}

LazyDatabase _openConnection() {
  return LazyDatabase(() async {
    final dbFolder = await getApplicationDocumentsDirectory();
    final file = File(p.join(dbFolder.path, 'db.sqlite'));
    return NativeDatabase.createInBackground(file);
  });
}
