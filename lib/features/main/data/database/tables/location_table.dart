import 'package:drift/drift.dart';

@DataClassName('Location')
class LocationTable extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get name => text().nullable()();
  TextColumn get region => text().nullable()();
  TextColumn get country => text().nullable()();
  RealColumn get lat => real().nullable()();
  RealColumn get lon => real().nullable()();
  TextColumn get tzId => text().nullable()();
  IntColumn get localtimeEpoch => integer().nullable()();
  TextColumn get localtime => text().nullable()();
}