import 'package:drift/drift.dart';
import 'package:weather_test_project/features/main/data/database/tables/condition_table.dart';

@DataClassName('Current')
class CurrentTable extends Table {
  IntColumn get id => integer().autoIncrement()();
  IntColumn get lastUpdatedEpoch => integer().nullable()();
  TextColumn get lastUpdated => text().nullable()();
  RealColumn get tempC => real().nullable()();
  RealColumn get tempF => real().nullable()();
  IntColumn get isDay => integer().nullable()();
  RealColumn get windMph => real().nullable()();
  RealColumn get windKph => real().nullable()();
  RealColumn get windDegree => real().nullable()();
  TextColumn get windDir => text().nullable()();
  RealColumn get pressureMb => real().nullable()();
  RealColumn get pressureIn => real().nullable()();
  RealColumn get precipMm => real().nullable()();
  RealColumn get precipIn => real().nullable()();
  RealColumn get humidity => real().nullable()();
  RealColumn get cloud => real().nullable()();
  RealColumn get feelslikeC => real().nullable()();
  RealColumn get feelslikeF => real().nullable()();
  RealColumn get visKm => real().nullable()();
  RealColumn get visMiles => real().nullable()();
  RealColumn get uv => real().nullable()();
  RealColumn get gustMph => real().nullable()();
  RealColumn get gustKph => real().nullable()();
  IntColumn get conditionId => integer().references(ConditionTable, #id)();
}