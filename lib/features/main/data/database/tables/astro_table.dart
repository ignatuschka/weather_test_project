import 'package:drift/drift.dart';

@DataClassName('Astro')
class AstroTable extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get sunrise => text().nullable()();
  TextColumn get sunset => text().nullable()();
  TextColumn get moonrise => text().nullable()();
  TextColumn get moonset => text().nullable()();
  TextColumn get moonPhase => text().nullable()();
  IntColumn get moonIllumination => integer().nullable()();
  IntColumn get isMoonUp => integer().nullable()();
  IntColumn get isSunUp => integer().nullable()();
}