import 'package:drift/drift.dart';

@DataClassName('Condition')
class ConditionTable extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get textCondition => text().nullable()();
  TextColumn get icon => text().nullable()();
  IntColumn get code => integer().nullable()();
}