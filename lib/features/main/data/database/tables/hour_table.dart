import 'package:drift/drift.dart';
import 'package:weather_test_project/features/main/data/database/tables/condition_table.dart';
import 'package:weather_test_project/features/main/data/database/tables/forecast_day_table.dart';

@DataClassName('Hour')
class HourTable extends Table {
  IntColumn get id => integer().autoIncrement()();
  IntColumn get timeEpoch => integer().nullable()();
  TextColumn get time => text().nullable()();
  RealColumn get tempC => real().nullable()();
  RealColumn get tempF => real().nullable()();
  IntColumn get isDay => integer().nullable()();
  RealColumn get windMph => real().nullable()();
  RealColumn get windKph => real().nullable()();
  RealColumn get windDegree => real().nullable()();
  TextColumn get windDir => text().nullable()();
  RealColumn get pressureMb => real().nullable()();
  RealColumn get pressureIn => real().nullable()();
  RealColumn get precipMm => real().nullable()();
  RealColumn get precipIn => real().nullable()();
  RealColumn get snowCm => real().nullable()();
  RealColumn get humidity => real().nullable()();
  RealColumn get cloud => real().nullable()();
  RealColumn get feelslikeC => real().nullable()();
  RealColumn get feelslikeF => real().nullable()();
  RealColumn get windchillC => real().nullable()();
  RealColumn get windchillF => real().nullable()();
  RealColumn get heatindexC => real().nullable()();
  RealColumn get heatindexF => real().nullable()();
  RealColumn get dewpointC => real().nullable()();
  RealColumn get dewpointF => real().nullable()();
  IntColumn get willItRain => integer().nullable()();
  RealColumn get chanceOfRain => real().nullable()();
  IntColumn get willItSnow => integer().nullable()();
  RealColumn get chanceOfSnow => real().nullable()();
  RealColumn get visKm => real().nullable()();
  RealColumn get visMiles => real().nullable()();
  RealColumn get gustMph => real().nullable()();
  RealColumn get gustKph => real().nullable()();
  RealColumn get uv => real().nullable()();
  IntColumn get forecastdayId => integer().references(ForecastdayTable, #id)();
  IntColumn get conditionId => integer().references(ConditionTable, #id)();
}