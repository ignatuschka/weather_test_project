import 'package:drift/drift.dart';
import 'package:weather_test_project/features/main/data/database/tables/astro_table.dart';
import 'package:weather_test_project/features/main/data/database/tables/day_table.dart';

@DataClassName('Forecastday')
class ForecastdayTable extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get date => text().nullable()();
  IntColumn get dateEpoch => integer().nullable()();
  IntColumn get dayId => integer().references(DayTable, #id)();
  IntColumn get astroId => integer().references(AstroTable, #id)();
}