import 'package:drift/drift.dart';
import 'package:weather_test_project/features/main/data/database/tables/condition_table.dart';

@DataClassName('Day')
class DayTable extends Table {
  IntColumn get id => integer().autoIncrement()();
  RealColumn get maxtempC => real().nullable()();
  RealColumn get maxtempF => real().nullable()();
  RealColumn get mintempC => real().nullable()();
  RealColumn get mintempF => real().nullable()();
  RealColumn get avgtempC => real().nullable()();
  RealColumn get avgtempF => real().nullable()();
  RealColumn get maxwindMph => real().nullable()();
  RealColumn get maxwindKph => real().nullable()();
  RealColumn get totalprecipMm => real().nullable()();
  RealColumn get totalprecipIn => real().nullable()();
  RealColumn get totalsnowCm => real().nullable()();
  RealColumn get avgvisKm => real().nullable()();
  RealColumn get avgvisMiles => real().nullable()();
  RealColumn get avghumidity => real().nullable()();
  IntColumn get dailyWillItRain => integer().nullable()();
  RealColumn get dailyChanceOfRain => real().nullable()();
  IntColumn get dailyWillItSnow => integer().nullable()();
  RealColumn get dailyChanceOfSnow => real().nullable()();
  RealColumn get uv => real().nullable()();
  IntColumn get conditionId => integer().references(ConditionTable, #id)();
}