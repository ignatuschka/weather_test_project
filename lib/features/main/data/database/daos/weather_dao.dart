import 'package:drift/drift.dart';
import 'package:weather_test_project/features/main/data/database/database.dart';
import 'package:weather_test_project/features/main/data/database/tables/astro_table.dart';
import 'package:weather_test_project/features/main/data/database/tables/condition_table.dart';
import 'package:weather_test_project/features/main/data/database/tables/current_table.dart';
import 'package:weather_test_project/features/main/data/database/tables/day_table.dart';
import 'package:weather_test_project/features/main/data/database/tables/forecast_day_table.dart';
import 'package:weather_test_project/features/main/data/database/tables/hour_table.dart';
import 'package:weather_test_project/features/main/data/database/tables/location_table.dart';
import 'package:weather_test_project/features/main/data/models/astro_model.dart';
import 'package:weather_test_project/features/main/data/models/condition_model.dart';
import 'package:weather_test_project/features/main/data/models/current_model.dart';
import 'package:weather_test_project/features/main/data/models/day_model.dart';
import 'package:weather_test_project/features/main/data/models/forecast_day_model.dart';
import 'package:weather_test_project/features/main/data/models/forecast_model.dart';
import 'package:weather_test_project/features/main/data/models/hour_model.dart';
import 'package:weather_test_project/features/main/data/models/location_model.dart';
import 'package:weather_test_project/features/main/data/models/weather_model.dart';

part 'weather_dao.g.dart';

@DriftAccessor(
  tables: [LocationTable, HourTable, ForecastdayTable, DayTable, CurrentTable, ConditionTable, AstroTable],
)
class WeatherDao extends DatabaseAccessor<AppDb> with _$WeatherDaoMixin {
  WeatherDao(AppDb db) : super(db);

  Future<void> createOrUpdateWeather({required WeatherModel model}) async {
    await _createOrUpdateLocation(model: model.location);
    await _createOrUpdateCurrent(model: model.current);
    await _createOrUpdateForecastDay(model: model.forecast?.forecastday?[0]);
  }

  Future<void> _createOrUpdateLocation({required LocationModel? model}) async {
    final existLocation = await select(locationTable).getSingleOrNull();
    if (existLocation == null) {
      final locationsToInsert = LocationTableCompanion(
        name: Value(model?.name),
        lat: Value(model?.lat),
        lon: Value(model?.lon),
        region: Value(model?.region),
        country: Value(model?.country),
        localtimeEpoch: Value(model?.localtimeEpoch),
        tzId: Value(model?.tzId),
        localtime: Value(model?.localtime),
      );
      await into(locationTable).insert(locationsToInsert);
    } else {
      final locationsToInsert = LocationTableCompanion(
        id: Value(existLocation.id),
        name: Value(model?.name),
        lat: Value(model?.lat),
        lon: Value(model?.lon),
        region: Value(model?.region),
        country: Value(model?.country),
        localtimeEpoch: Value(model?.localtimeEpoch),
        tzId: Value(model?.tzId),
        localtime: Value(model?.localtime),
      );
      update(locationTable).replace((locationsToInsert));
    }
  }

  Future<void> _createOrUpdateCurrent({required CurrentModel? model}) async {
    final existCurrent = await select(currentTable).getSingleOrNull();
    if (existCurrent == null) {
      final currentCondition = ConditionTableCompanion(
          textCondition: Value(model?.condition?.text),
          icon: Value(model?.condition?.icon),
          code: Value(model?.condition?.code));
      final savedCondition = await into(conditionTable).insert(currentCondition);
      final currentsToInsert = CurrentTableCompanion(
        lastUpdatedEpoch: Value(model?.lastUpdatedEpoch),
        lastUpdated: Value(model?.lastUpdated),
        tempC: Value(model?.tempC),
        tempF: Value(model?.tempF),
        isDay: Value(model?.isDay),
        windMph: Value(model?.windMph),
        windKph: Value(model?.windKph),
        windDir: Value(model?.windDir),
        windDegree: Value(model?.windDegree),
        pressureMb: Value(model?.pressureMb),
        pressureIn: Value(model?.pressureIn),
        precipMm: Value(model?.precipMm),
        precipIn: Value(model?.precipIn),
        humidity: Value(model?.humidity),
        cloud: Value(model?.cloud),
        feelslikeC: Value(model?.feelslikeC),
        feelslikeF: Value(model?.feelslikeF),
        visKm: Value(model?.visKm),
        visMiles: Value(model?.visMiles),
        uv: Value(model?.uv),
        gustKph: Value(model?.gustKph),
        gustMph: Value(model?.gustMph),
        conditionId: Value(savedCondition),
      );
      await into(currentTable).insert(currentsToInsert);
    } else {
      final currentCondition = ConditionTableCompanion(
          id: Value(existCurrent.conditionId),
          textCondition: Value(model?.condition?.text),
          icon: Value(model?.condition?.icon),
          code: Value(model?.condition?.code));
      final currentConditionId =
          await (update(conditionTable)..where((tbl) => tbl.id.equals(existCurrent.conditionId))).write(currentCondition);
      final currentsToInsert = CurrentTableCompanion(
        id: Value(existCurrent.id),
        lastUpdatedEpoch: Value(model?.lastUpdatedEpoch),
        lastUpdated: Value(model?.lastUpdated),
        tempC: Value(model?.tempC),
        tempF: Value(model?.tempF),
        isDay: Value(model?.isDay),
        windMph: Value(model?.windMph),
        windKph: Value(model?.windKph),
        windDir: Value(model?.windDir),
        windDegree: Value(model?.windDegree),
        pressureMb: Value(model?.pressureMb),
        pressureIn: Value(model?.pressureIn),
        precipMm: Value(model?.precipMm),
        precipIn: Value(model?.precipIn),
        humidity: Value(model?.humidity),
        cloud: Value(model?.cloud),
        feelslikeC: Value(model?.feelslikeC),
        feelslikeF: Value(model?.feelslikeF),
        visKm: Value(model?.visKm),
        visMiles: Value(model?.visMiles),
        uv: Value(model?.uv),
        gustKph: Value(model?.gustKph),
        gustMph: Value(model?.gustMph),
        conditionId: Value(currentConditionId),
      );
      update(currentTable).replace((currentsToInsert));
    }
  }

  Future<void> _createOrUpdateForecastDay({required ForecastDayModel? model}) async {
    final existForecastday = await select(forecastdayTable).getSingleOrNull();
    if (existForecastday == null) {
      final astroToInsert = AstroTableCompanion(
        sunrise: Value(model?.astro?.sunrise),
        sunset: Value(model?.astro?.sunset),
        moonrise: Value(model?.astro?.moonrise),
        moonset: Value(model?.astro?.moonset),
        moonPhase: Value(model?.astro?.moonPhase),
        moonIllumination: Value(model?.astro?.moonIllumination),
        isMoonUp: Value(model?.astro?.isMoonUp),
        isSunUp: Value(model?.astro?.isSunUp),
      );
      final astroId = await into(astroTable).insert(astroToInsert);
      final dayCondition = ConditionTableCompanion(
        textCondition: Value(model?.day?.condition?.text),
        icon: Value(model?.day?.condition?.icon),
        code: Value(model?.day?.condition?.code),
      );
      final dayConditionId = await into(conditionTable).insert(dayCondition);
      final dayToInsert = DayTableCompanion(
        maxtempC: Value(model?.day?.maxtempC),
        maxtempF: Value(model?.day?.maxtempF),
        mintempC: Value(model?.day?.mintempC),
        mintempF: Value(model?.day?.mintempF),
        avgtempC: Value(model?.day?.avgtempC),
        avgtempF: Value(model?.day?.avgtempF),
        maxwindMph: Value(model?.day?.maxwindMph),
        maxwindKph: Value(model?.day?.maxwindKph),
        totalprecipMm: Value(model?.day?.totalprecipMm),
        totalprecipIn: Value(model?.day?.totalprecipIn),
        totalsnowCm: Value(model?.day?.totalsnowCm),
        avgvisKm: Value(model?.day?.avgvisKm),
        avgvisMiles: Value(model?.day?.avgvisMiles),
        avghumidity: Value(model?.day?.avghumidity),
        dailyWillItRain: Value(model?.day?.dailyWillItRain),
        dailyChanceOfRain: Value(model?.day?.dailyChanceOfRain),
        dailyWillItSnow: Value(model?.day?.dailyWillItSnow),
        dailyChanceOfSnow: Value(model?.day?.dailyChanceOfSnow),
        uv: Value(model?.day?.uv),
        conditionId: Value(dayConditionId),
      );
      final dayId = await into(dayTable).insert(dayToInsert);
      final forecastday = ForecastdayTableCompanion(
        date: Value(model?.date),
        dateEpoch: Value(model?.dateEpoch),
        dayId: Value(dayId),
        astroId: Value(astroId),
      );
      final forecastdayId = await into(forecastdayTable).insert(forecastday);
      final List<HourTableCompanion> listHourToInsert = [];
      if (model != null && model.hour != null) {
        final List<HourModel> listHours = model.hour ?? [];
        for (var hour in listHours) {
          final hourConditionToInsert = ConditionTableCompanion(
            textCondition: Value(hour.condition?.text),
            icon: Value(hour.condition?.icon),
            code: Value(hour.condition?.code),
          );
          final hourConditionId = await into(conditionTable).insert(hourConditionToInsert);
          final hourToInsert = HourTableCompanion(
            timeEpoch: Value(hour.timeEpoch),
            time: Value(hour.time),
            tempC: Value(hour.tempC),
            tempF: Value(hour.tempF),
            isDay: Value(hour.isDay),
            windMph: Value(hour.windMph),
            windKph: Value(hour.windKph),
            windDegree: Value(hour.windDegree),
            windDir: Value(hour.windDir),
            pressureMb: Value(hour.pressureMb),
            pressureIn: Value(hour.pressureIn),
            precipMm: Value(hour.precipMm),
            precipIn: Value(hour.precipIn),
            snowCm: Value(hour.snowCm),
            humidity: Value(hour.humidity),
            cloud: Value(hour.cloud),
            feelslikeC: Value(hour.feelslikeC),
            feelslikeF: Value(hour.feelslikeF),
            windchillC: Value(hour.windchillC),
            windchillF: Value(hour.windchillF),
            heatindexC: Value(hour.heatindexC),
            heatindexF: Value(hour.heatindexF),
            dewpointC: Value(hour.dewpointC),
            dewpointF: Value(hour.dewpointF),
            willItRain: Value(hour.willItRain),
            chanceOfRain: Value(hour.chanceOfRain),
            willItSnow: Value(hour.willItSnow),
            chanceOfSnow: Value(hour.chanceOfSnow),
            visKm: Value(hour.visKm),
            visMiles: Value(hour.visMiles),
            gustMph: Value(hour.gustMph),
            gustKph: Value(hour.gustKph),
            uv: Value(hour.uv),
            forecastdayId: Value(forecastdayId),
            conditionId: Value(hourConditionId),
          );
          listHourToInsert.add(hourToInsert);
        }
        await batch((batch) {
          batch.insertAll(hourTable, listHourToInsert);
        });
      }
    } else {
      final forecastAstro = AstroTableCompanion(
        id: Value(existForecastday.astroId),
        sunrise: Value(model?.astro?.sunrise),
        sunset: Value(model?.astro?.sunset),
        moonrise: Value(model?.astro?.moonrise),
        moonset: Value(model?.astro?.moonset),
        moonPhase: Value(model?.astro?.moonPhase),
        moonIllumination: Value(model?.astro?.moonIllumination),
        isMoonUp: Value(model?.astro?.isMoonUp),
        isSunUp: Value(model?.astro?.isSunUp),
      );
      final astroId =
          await (update(astroTable)..where((tbl) => tbl.id.equals(existForecastday.astroId))).write(forecastAstro);
      final day = await (select(dayTable)..where((tbl) => tbl.id.equals(existForecastday.dayId))).getSingleOrNull();
      final dayCondition = ConditionTableCompanion(
        id: Value(day?.conditionId ?? 0),
        textCondition: Value(model?.day?.condition?.text),
        icon: Value(model?.day?.condition?.icon),
        code: Value(model?.day?.condition?.code),
      );
      final dayConditionId =
          await (update(conditionTable)..where((tbl) => tbl.id.equals(day?.conditionId ?? 0))).write(dayCondition);
      final forecastdayDay = DayTableCompanion(
        id: Value(existForecastday.dayId),
        maxtempC: Value(model?.day?.maxtempC),
        maxtempF: Value(model?.day?.maxtempF),
        mintempC: Value(model?.day?.mintempC),
        mintempF: Value(model?.day?.mintempF),
        avgtempC: Value(model?.day?.avgtempC),
        avgtempF: Value(model?.day?.avgtempF),
        maxwindMph: Value(model?.day?.maxwindMph),
        maxwindKph: Value(model?.day?.maxwindKph),
        totalprecipMm: Value(model?.day?.totalprecipMm),
        totalprecipIn: Value(model?.day?.totalprecipIn),
        totalsnowCm: Value(model?.day?.totalsnowCm),
        avgvisKm: Value(model?.day?.avgvisKm),
        avgvisMiles: Value(model?.day?.avgvisMiles),
        avghumidity: Value(model?.day?.avghumidity),
        dailyWillItRain: Value(model?.day?.dailyWillItRain),
        dailyChanceOfRain: Value(model?.day?.dailyChanceOfRain),
        dailyWillItSnow: Value(model?.day?.dailyWillItSnow),
        dailyChanceOfSnow: Value(model?.day?.dailyChanceOfSnow),
        uv: Value(model?.day?.uv),
        conditionId: Value(dayConditionId),
      );
      final dayId = await (update(dayTable)..where((tbl) => tbl.id.equals(existForecastday.dayId))).write(forecastdayDay);
      final forecastday = ForecastdayTableCompanion(
        id: Value(existForecastday.id),
        date: Value(model?.date),
        dateEpoch: Value(model?.dateEpoch),
        dayId: Value(dayId),
        astroId: Value(astroId),
      );
      await update(forecastdayTable).replace(forecastday);
      final listHour =
          await (select(hourTable)..where((tbl) => tbl.forecastdayId.equals(existForecastday.id))).get();
      final List<HourTableCompanion> hoursToUpdate = [];
      for (int i = 0; i < (model?.hour?.length ?? 24); i++) {
        final hourCondition = ConditionTableCompanion(
            id: Value(listHour[i].conditionId),
            textCondition: Value(model?.hour?[i].condition?.text),
            icon: Value(model?.hour?[i].condition?.icon),
            code: Value(model?.hour?[i].condition?.code));
        final hourConditionId =
            await (update(conditionTable)..where((tbl) => tbl.id.equals(listHour[i].conditionId))).write(hourCondition);
        final hourToUpdate = HourTableCompanion(
          id: Value(listHour[i].id),
          timeEpoch: Value(model?.hour?[i].timeEpoch),
          time: Value(model?.hour?[i].time),
          tempC: Value(model?.hour?[i].tempC),
          tempF: Value(model?.hour?[i].tempF),
          isDay: Value(model?.hour?[i].isDay),
          windMph: Value(model?.hour?[i].windMph),
          windKph: Value(model?.hour?[i].windKph),
          windDegree: Value(model?.hour?[i].windDegree),
          windDir: Value(model?.hour?[i].windDir),
          pressureMb: Value(model?.hour?[i].pressureMb),
          pressureIn: Value(model?.hour?[i].pressureIn),
          precipMm: Value(model?.hour?[i].precipMm),
          precipIn: Value(model?.hour?[i].precipIn),
          snowCm: Value(model?.hour?[i].snowCm),
          humidity: Value(model?.hour?[i].humidity),
          cloud: Value(model?.hour?[i].cloud),
          feelslikeC: Value(model?.hour?[i].feelslikeC),
          feelslikeF: Value(model?.hour?[i].feelslikeF),
          windchillC: Value(model?.hour?[i].windchillC),
          windchillF: Value(model?.hour?[i].windchillF),
          heatindexC: Value(model?.hour?[i].heatindexC),
          heatindexF: Value(model?.hour?[i].heatindexF),
          dewpointC: Value(model?.hour?[i].dewpointC),
          dewpointF: Value(model?.hour?[i].dewpointF),
          willItRain: Value(model?.hour?[i].willItRain),
          chanceOfRain: Value(model?.hour?[i].chanceOfRain),
          willItSnow: Value(model?.hour?[i].willItSnow),
          chanceOfSnow: Value(model?.hour?[i].chanceOfSnow),
          visKm: Value(model?.hour?[i].visKm),
          visMiles: Value(model?.hour?[i].visMiles),
          gustMph: Value(model?.hour?[i].gustMph),
          gustKph: Value(model?.hour?[i].gustKph),
          uv: Value(model?.hour?[i].uv),
          forecastdayId: Value(existForecastday.id),
          conditionId: Value(hourConditionId),
        );
        hoursToUpdate.add(hourToUpdate);
      }
      await batch((batch) {
        batch.replaceAll(hourTable, hoursToUpdate);
      });
    }
  }

  Future<WeatherModel> getWeatherForecast() async {
    final dbLocations = await select(locationTable).getSingleOrNull();
    final dbCurrent = await select(currentTable).getSingleOrNull();
    final dbCurrentCondition =
        await (select(conditionTable)..where((tbl) => tbl.id.equals(dbCurrent?.conditionId ?? 0))).getSingleOrNull();
    final dbForecastday = await select(forecastdayTable).getSingleOrNull();
    final dbDay = await select(dayTable).getSingleOrNull();
    final dbDayCondition =
        await (select(conditionTable)..where((tbl) => tbl.id.equals(dbDay?.conditionId ?? 0))).getSingleOrNull();
    final dbAstro = await select(astroTable).getSingleOrNull();
    final dbHour = await select(hourTable).get();
    final List<Condition?> listHourCondition = [];
    for (var element in dbHour) {
      final dbHourCondition =
          await (select(conditionTable)..where((tbl) => tbl.id.equals(element.conditionId))).getSingleOrNull();
      listHourCondition.add(dbHourCondition);
    }
    final location = LocationModel(
      name: dbLocations?.name,
      region: dbLocations?.region,
      country: dbLocations?.country,
      lat: dbLocations?.lat,
      lon: dbLocations?.lon,
      tzId: dbLocations?.tzId,
      localtimeEpoch: dbLocations?.localtimeEpoch,
      localtime: dbLocations?.localtime,
    );
    final currentCondition = ConditionModel(
      text: dbCurrentCondition?.textCondition,
      icon: dbCurrentCondition?.icon,
      code: dbCurrentCondition?.code,
    );
    final current = CurrentModel(
      lastUpdatedEpoch: dbCurrent?.lastUpdatedEpoch,
      lastUpdated: dbCurrent?.lastUpdated,
      tempC: dbCurrent?.tempC,
      tempF: dbCurrent?.tempF,
      isDay: dbCurrent?.isDay,
      condition: currentCondition,
      windMph: dbCurrent?.windMph,
      windKph: dbCurrent?.windKph,
      windDegree: dbCurrent?.windDegree,
      windDir: dbCurrent?.windDir,
      pressureMb: dbCurrent?.pressureMb,
      pressureIn: dbCurrent?.pressureIn,
      precipMm: dbCurrent?.precipMm,
      precipIn: dbCurrent?.precipIn,
      humidity: dbCurrent?.humidity,
      cloud: dbCurrent?.cloud,
      feelslikeC: dbCurrent?.feelslikeC,
      feelslikeF: dbCurrent?.feelslikeF,
      visKm: dbCurrent?.visKm,
      visMiles: dbCurrent?.visMiles,
      uv: dbCurrent?.uv,
      gustMph: dbCurrent?.gustMph,
      gustKph: dbCurrent?.gustKph,
    );
    final dayCondition = ConditionModel(
      text: dbDayCondition?.textCondition,
      icon: dbDayCondition?.icon,
      code: dbDayCondition?.code,
    );
    final day = DayModel(
      maxtempC: dbDay?.maxtempC,
      maxtempF: dbDay?.maxtempF,
      mintempC: dbDay?.mintempC,
      mintempF: dbDay?.mintempF,
      avgtempC: dbDay?.avgtempC,
      avgtempF: dbDay?.avgtempF,
      maxwindMph: dbDay?.maxwindMph,
      maxwindKph: dbDay?.maxwindKph,
      totalprecipMm: dbDay?.totalprecipMm,
      totalprecipIn: dbDay?.totalprecipIn,
      totalsnowCm: dbDay?.totalsnowCm,
      avgvisKm: dbDay?.avgvisKm,
      avgvisMiles: dbDay?.avgvisMiles,
      avghumidity: dbDay?.avghumidity,
      dailyWillItRain: dbDay?.dailyWillItRain,
      dailyChanceOfRain: dbDay?.dailyChanceOfRain,
      dailyWillItSnow: dbDay?.dailyWillItSnow,
      dailyChanceOfSnow: dbDay?.dailyChanceOfSnow,
      condition: dayCondition,
      uv: dbDay?.uv,
    );
    final astro = AstroModel(
      sunrise: dbAstro?.sunrise,
      sunset: dbAstro?.sunset,
      moonrise: dbAstro?.moonrise,
      moonset: dbAstro?.moonset,
      moonPhase: dbAstro?.moonPhase,
      moonIllumination: dbAstro?.moonIllumination,
      isMoonUp: dbAstro?.isMoonUp,
      isSunUp: dbAstro?.isSunUp,
    );
    final List<HourModel> listHourEntity = [];
    for (int i = 0; i < dbHour.length; i++) {
      final hourCondition = ConditionModel(
        text: listHourCondition[i]?.textCondition,
        icon: listHourCondition[i]?.icon,
        code: listHourCondition[i]?.code,
      );
      final hourEntity = HourModel(
        timeEpoch: dbHour[i].timeEpoch,
        time: dbHour[i].time,
        tempC: dbHour[i].tempC,
        tempF: dbHour[i].tempF,
        isDay: dbHour[i].isDay,
        condition: hourCondition,
        windMph: dbHour[i].windMph,
        windKph: dbHour[i].windKph,
        windDegree: dbHour[i].windDegree,
        windDir: dbHour[i].windDir,
        pressureMb: dbHour[i].pressureMb,
        pressureIn: dbHour[i].pressureIn,
        precipMm: dbHour[i].precipMm,
        precipIn: dbHour[i].precipIn,
        snowCm: dbHour[i].snowCm,
        humidity: dbHour[i].humidity,
        cloud: dbHour[i].cloud,
        feelslikeC: dbHour[i].feelslikeC,
        feelslikeF: dbHour[i].feelslikeF,
        windchillC: dbHour[i].windchillC,
        windchillF: dbHour[i].windchillF,
        heatindexC: dbHour[i].heatindexC,
        heatindexF: dbHour[i].heatindexF,
        dewpointC: dbHour[i].dewpointC,
        dewpointF: dbHour[i].dewpointF,
        willItRain: dbHour[i].willItRain,
        chanceOfRain: dbHour[i].chanceOfRain,
        willItSnow: dbHour[i].willItSnow,
        chanceOfSnow: dbHour[i].chanceOfSnow,
        visKm: dbHour[i].visKm,
        visMiles: dbHour[i].visMiles,
        gustMph: dbHour[i].gustMph,
        gustKph: dbHour[i].gustKph,
        uv: dbHour[i].uv,
      );
      listHourEntity.add(hourEntity);
    }
    final forecastday = ForecastDayModel(
      date: dbForecastday?.date,
      dateEpoch: dbForecastday?.dateEpoch,
      day: day,
      astro: astro,
      hour: listHourEntity,
    );
    final forecast = ForecastModel(forecastday: [forecastday]);
    final weatherEntity = WeatherModel(
      location: location,
      current: current,
      forecast: forecast,
    );
    return weatherEntity;
  }
}
