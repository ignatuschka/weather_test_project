import 'package:geolocator/geolocator.dart';
import 'package:location/location.dart';
import 'package:weather_test_project/core/core_exceptions.dart';

abstract class LocationService {
  Future<bool> requestPermission();
  Future<LocationData> getCurrentLocation();
  Future<void> openAppSettings();
  Future<void> openLocationSettings();
}

class LocationServiceImpl implements LocationService {
  final Location location;
  final GeolocatorPlatform geolocator;
  LocationServiceImpl({
    required this.location,
    required this.geolocator,
  });

  @override
  Future<LocationData> getCurrentLocation() async {
    final serviceEnabled = await location.serviceEnabled();
    if (!serviceEnabled) {
      final result = await location.requestService();
      if (result != true) {
        throw const LocatioServiceNotEnabledException();
      }
    }
    final locationData = await location.getLocation();
    return locationData;
  }

  @override
  Future<bool> requestPermission() async {
    PermissionStatus permission = await location.hasPermission();
    if (permission == PermissionStatus.denied || permission == PermissionStatus.deniedForever) {
      permission = await location.requestPermission();
    }
    if (permission == PermissionStatus.denied || permission == PermissionStatus.deniedForever) {
      throw const PermissionAccessException();
    }
    return permission == PermissionStatus.granted || permission == PermissionStatus.grantedLimited;
  }
  
  @override
  Future<void> openAppSettings() async {
    await geolocator.openAppSettings();
  }

  @override
  Future<void> openLocationSettings() async {
    await geolocator.openLocationSettings();
  }
}
