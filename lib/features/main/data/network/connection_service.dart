import 'package:connectivity_plus/connectivity_plus.dart';

abstract class ConnectionService {
  Future<bool> checkConnection();
}

class ConnectionServiceImpl implements ConnectionService {
  final Connectivity connectivity;
  ConnectionServiceImpl({
    required this.connectivity,
  });

  @override
  Future<bool> checkConnection() async {
    final connectivityResult = await connectivity.checkConnectivity();
    return connectivityResult != ConnectivityResult.none;
  }
}
