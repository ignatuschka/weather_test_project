import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';
import 'package:weather_test_project/features/main/data/models/weather_model.dart';

part 'main_api.g.dart';

@RestApi(baseUrl: 'https://api.weatherapi.com')
abstract class MainApi {
  factory MainApi(Dio dio, {String baseUrl}) = _MainApi;

  @GET('/v1/forecast.json')
  Future<WeatherModel> getWeather(
    @Query('q') String q
  );
}
