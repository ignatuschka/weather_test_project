import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get_it/get_it.dart';
import 'package:location/location.dart' as location;

import 'package:weather_test_project/core/dio_module.dart';
import 'package:weather_test_project/features/main/data/data_source/main_local_data_source.dart';
import 'package:weather_test_project/features/main/data/data_source/main_remote_data_source.dart';
import 'package:weather_test_project/features/main/data/database/daos/weather_dao.dart';
import 'package:weather_test_project/features/main/data/database/database.dart';
import 'package:weather_test_project/features/main/data/network/connection_service.dart';

import 'package:weather_test_project/features/main/data/network/location_service.dart';
import 'package:weather_test_project/features/main/data/network/main_api.dart';
import 'package:weather_test_project/features/main/domain/entity/hour_entity.dart';
import 'package:weather_test_project/features/main/domain/usecase/check_connection_usecase.dart';
import 'package:weather_test_project/features/main/domain/usecase/get_current_location_usecase.dart';
import 'package:weather_test_project/features/main/domain/usecase/get_weather_usecase.dart';
import 'package:weather_test_project/features/main/domain/usecase/get_db_weather_usecase.dart';
import 'package:weather_test_project/features/main/domain/usecase/create_update_weather_usecase.dart';
import 'package:weather_test_project/features/main/domain/usecase/open_app_settings_usecase.dart';
import 'package:weather_test_project/features/main/domain/usecase/open_location_settings_usecase.dart';
import 'package:weather_test_project/features/main/domain/usecase/request_permission_usecase.dart';
import 'package:weather_test_project/features/main/presentation/screens/main_screen/bloc/main_bloc.dart';
import 'package:weather_test_project/features/main/presentation/screens/main_screen/main_screen.dart';
import 'package:weather_test_project/features/main/presentation/widgets/weather_list_item/bloc/weather_list_item_bloc.dart';
import 'package:weather_test_project/features/main/presentation/widgets/weather_list_item/weather_list_item.dart';

import '../data/repository/main_repository_impl.dart';
import '../domain/repository/main_repository.dart';

class MainDIModule {
  void updateInjections(GetIt instance) {
    instance.registerLazySingleton<MainRemoteDataSource>(() => MainRemoteDataSourceImpl(
          locationService: instance.get(),
          api: instance.get(),
          connectionService: instance.get(),
        ));

    instance.registerSingleton<AppDb>(AppDb());

    instance.registerLazySingleton(() => WeatherDao(instance.get()));

    instance.registerLazySingleton<MainLocalDataSource>(() => MainLocalDataSourceImpl(
          weatherDao: instance.get(),
        ));

    instance.registerSingleton<Connectivity>(Connectivity());

    instance.registerLazySingleton<ConnectionService>(() => ConnectionServiceImpl(connectivity: instance.get()));

    instance.registerSingleton<location.Location>(location.Location.instance);

    instance.registerSingleton<GeolocatorPlatform>(GeolocatorPlatform.instance);

    instance.registerLazySingleton<LocationService>(() => LocationServiceImpl(
          location: instance.get(),
          geolocator: instance.get(),
        ));

    instance.registerSingleton<Dio>(DioModule.provideDio());

    instance.registerFactory(() => MainApi(instance.get()));

    instance.registerLazySingleton<MainRepository>(() => MainRepositoryImpl(
          remoteDataSource: instance.get(),
          localDataSource: instance.get(),
        ));

    instance.registerFactory(() => GetCurrentLocationUsecase(repository: instance.get()));

    instance.registerFactory(() => GetWeatherUsecase(repository: instance.get()));

    instance.registerFactory(() => RequestPermissionUsecase(repository: instance.get()));

    instance.registerFactory(() => CreateOrUpdateWeatherUsecase(repository: instance.get()));

    instance.registerFactory(() => GetDbWeatherUsecase(repository: instance.get()));

    instance.registerFactory(() => CheckConnectionUsecase(repository: instance.get()));

    instance.registerFactory(() => OpenLocationSettingsUsecase(repository: instance.get()));

    instance.registerFactory(() => OpenAppSettingsUsecase(repository: instance.get()));
  }
}

class MainPage extends StatelessWidget {
  const MainPage({super.key});

  GetIt get getIt => GetIt.I;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => MainBloc(
        getCurrentLocation: getIt(),
        getWeather: getIt(),
        requestPermission: getIt(),
        getDbWeather: getIt(),
        createOrUpdateWeather: getIt(),
        checkConnection: getIt(),
        openAppSettingsUsecase: getIt(),
        openLocationSettingsUsecase: getIt(),
      ),
      child: const MainScreen(),
    );
  }
}

class WeatherListItemPage extends StatelessWidget {
  final HourEntity? hourEntity;
  const WeatherListItemPage({
    Key? key,
    required this.hourEntity,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => WeatherListItemBloc(hourEntity: hourEntity),
      child: const WeatherListItem(),
    );
  }
}
