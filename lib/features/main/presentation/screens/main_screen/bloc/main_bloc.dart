import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rxdart/rxdart.dart';
import 'package:weather_test_project/core/base_usecase.dart';
import 'package:weather_test_project/core/globals.dart';
import 'package:weather_test_project/features/main/domain/usecase/check_connection_usecase.dart';
import 'package:weather_test_project/features/main/domain/usecase/create_update_weather_usecase.dart';

import 'package:weather_test_project/features/main/domain/usecase/get_current_location_usecase.dart';
import 'package:weather_test_project/features/main/domain/usecase/get_db_weather_usecase.dart';
import 'package:weather_test_project/features/main/domain/usecase/get_weather_usecase.dart';
import 'package:weather_test_project/features/main/domain/usecase/open_app_settings_usecase.dart';
import 'package:weather_test_project/features/main/domain/usecase/open_location_settings_usecase.dart';
import 'package:weather_test_project/features/main/domain/usecase/request_permission_usecase.dart';
import 'package:weather_test_project/features/main/presentation/screens/main_screen/bloc/main_event.dart';
import 'package:weather_test_project/features/main/presentation/screens/main_screen/bloc/main_state.dart';

class MainBloc extends Bloc<MainEvent, MainState> {
  final GetCurrentLocationUsecase getCurrentLocation;
  final GetWeatherUsecase getWeather;
  final RequestPermissionUsecase requestPermission;
  final GetDbWeatherUsecase getDbWeather;
  final CreateOrUpdateWeatherUsecase createOrUpdateWeather;
  final CheckConnectionUsecase checkConnection;
  final OpenAppSettingsUsecase openAppSettingsUsecase;
  final OpenLocationSettingsUsecase openLocationSettingsUsecase;

  MainBloc({
    required this.getCurrentLocation,
    required this.getWeather,
    required this.requestPermission,
    required this.getDbWeather,
    required this.createOrUpdateWeather,
    required this.checkConnection,
    required this.openAppSettingsUsecase,
    required this.openLocationSettingsUsecase,
  }) : super(MainState.initial()) {
    on<MainInitialEvent>(_initial, transformer: _debounceSequential(const Duration(milliseconds: 1000)));
    add(MainInitialEvent());
  }

  EventTransformer<Event> _debounceSequential<Event>(Duration duration) {
    return (events, mapper) => events.throttleTime(duration).asyncExpand((mapper));
  }

  Future<void> _initial(MainEvent event, Emitter<MainState> emit) async {
    emit(state.setIsError(false).setIsLoadion(true));
    await processUseCase(
      () async => await checkConnection(EmptyParams()),
      onSucess: (result) async {
        if (!result) {
          await processUseCase(
            () async => await getDbWeather(EmptyParams()),
            onSucess: (result) async {
              if ((result.current?.humidity ?? 0) < 40) {
                emit(state.setHumidity('Низкая влажность'));
              } else if ((result.current?.humidity ?? 0) > 60) {
                emit(state.setHumidity('Высокая влажность'));
              } else {
                emit(state.setHumidity('Средняя влажность'));
              }

              emit(state.setWeather(result));
              emit(state.setWindDir(windDir[state.weather?.current?.windDir] ?? ''));
              emit(state.setAssetPath(bigAssetPath[result.current?.condition?.code ?? 1000] ?? 'assets/BigSun.svg'));
              if (state.weather?.current?.lastUpdated != null) {
                await showCustomSnackBar(
                  event:
                      'Данные актуальны на ${state.weather?.current?.lastUpdated}. Чтобы получить актуальные данные, необходимо подключиться к сети.',
                  needAction: false,
                  isError: false,
                );
                emit(state.setIsLoadion(false));
              } else {
                await showCustomSnackBar(
                  event: 'Подключитесь к сети для получения прогноза погоды',
                  needAction: false,
                );
              }
            },
          );
        } else {
          await processUseCase(
            () async => await requestPermission(EmptyParams()),
            onSucess: (result) async {
              if (result) {
                await processUseCase(
                  () async => await getCurrentLocation(EmptyParams()),
                  onSucess: (result) async {
                    await processUseCase(
                      () async => await getWeather('${result.latitude},${result.longitude}'),
                      onSucess: (result) async {
                        if (result.current != null && result.current?.humidity != null) {
                          if (result.current!.humidity! < 40) {
                            emit(state.setHumidity('Низкая влажность'));
                          } else if (result.current!.humidity! > 60) {
                            emit(state.setHumidity('Высокая влажность'));
                          } else {
                            emit(state.setHumidity('Средняя влажность'));
                          }
                        }
                        emit(state.setWeather(result));
                        emit(state.setWindDir(windDir[state.weather?.current?.windDir] ?? ''));
                        emit(state.setAssetPath(
                            bigAssetPath[result.current?.condition?.code ?? 1000] ?? 'assets/BigSun.png'));
                        await processUseCase(
                          () async => await createOrUpdateWeather(result),
                          onSucess: (result) async {
                            await showCustomSnackBar(event: 'Данные обновлены', needAction: false, isError: false);
                          },
                        );
                        emit(state.setIsLoadion(false));
                      },
                    );
                  },
                  onError: (exception) async {
                    emit(state.setIsError(true));
                    await showCustomSnackBar(
                      event: exception,
                      label: 'Открыть Настройки',
                      needAction: true,
                      onTap: () async => await processUseCase(() async => await openLocationSettingsUsecase(EmptyParams())),
                    );
                  },
                );
              }
            },
            onError: (exception) async {
              emit(state.setIsError(true));
              await showCustomSnackBar(
                event: exception,
                label: 'Открыть Настройки',
                needAction: true,
                onTap: () async => await processUseCase(() async => await openAppSettingsUsecase(EmptyParams())),
              );
            },
          );
        }
      },
    );
  }
}
