import 'package:built_value/built_value.dart';
import 'package:weather_test_project/features/main/domain/entity/weather_entity.dart';

part 'main_state.g.dart';

abstract class MainState implements Built<MainState, MainStateBuilder> {
  bool get isLoading;
  WeatherEntity? get weather;
  String get humidity;
  String get windDir;
  String get assetPath;
  bool get isError;

  MainState._();

  factory MainState([void Function(MainStateBuilder)? updates]) = _$MainState;

  factory MainState.initial() => MainState((b) => b
    ..isLoading = true
    ..weather = null
    ..humidity = ''
    ..windDir = ''
    ..assetPath = ''
    ..isError = false);

  MainState setIsLoadion(bool isLoading) => rebuild((b) => (b)..isLoading = isLoading);

  MainState setWeather(WeatherEntity? weather) => rebuild((b) => (b)..weather = weather);

  MainState setHumidity(String? humidity) => rebuild((b) => (b)..humidity = humidity);

  MainState setWindDir(String windDir) => rebuild((b) => (b)..windDir = windDir);
  
  MainState setAssetPath(String assetPath) => rebuild((b) => (b)..assetPath = assetPath);

  MainState setIsError(bool isError) => rebuild((b) => (b)..isError = isError);
}
