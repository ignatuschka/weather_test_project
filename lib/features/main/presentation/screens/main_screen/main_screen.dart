import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_test_project/core/custom_fonts.dart';
import 'package:weather_test_project/core/theme/app_color_theme.dart';
import 'package:weather_test_project/features/auth/presentation/widgets/body_large.dart';
import 'package:weather_test_project/features/auth/presentation/widgets/body_medium_bold.dart';
import 'package:weather_test_project/features/auth/presentation/widgets/custom_loading_indicator.dart';
import 'package:weather_test_project/features/auth/presentation/widgets/display_large_bold.dart';
import 'package:weather_test_project/features/auth/presentation/widgets/heading_medium.dart';
import 'package:weather_test_project/features/main/presentation/screens/main_screen/bloc/main_bloc.dart';
import 'package:weather_test_project/features/main/presentation/screens/main_screen/bloc/main_event.dart';
import 'package:weather_test_project/features/main/presentation/screens/main_screen/bloc/main_state.dart';
import 'package:weather_test_project/features/main/presentation/widgets/blur_ellipse.dart';
import 'package:weather_test_project/features/main/presentation/widgets/weather_list.dart';
import 'package:weather_test_project/features/main/presentation/widgets/wind_humidity_card.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({super.key});

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MainBloc, MainState>(builder: (context, state) {
      return Scaffold(
        body: RefreshIndicator(
          onRefresh: () async => context.read<MainBloc>().add(MainInitialEvent()),
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment(2, 3),
                colors: <Color>[
                  Color(0xff130587),
                  Color(0xff180873),
                  Color(0xff1a0a5f),
                  Color(0xff1a0b4c),
                  Color(0xff180b3a),
                  Color(0xff160829),
                  Color(0xff0f0418),
                  Color(0xff000000),
                ],
              ),
            ),
            child: SingleChildScrollView(
              physics: const AlwaysScrollableScrollPhysics(),
              child: (state.isLoading && !state.isError)
                  ? Padding(
                      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height / 2),
                      child: const CustomLoadingIndicator(),
                    )
                  : (state.isError)
                      ? Padding(
                        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height / 2),
                        child: Center(
                          child: HeadingMedium(
                              text: 'Проследуйте по подсказке в всплывающем окне, а затем проведите вниз, чтобы обновить',
                              color: const LightColorTheme().white,
                              textAlign: TextAlign.center,
                            ),
                        ),
                      )
                      : Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 24),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 44, bottom: 24),
                                child: SizedBox(
                                  width: double.infinity,
                                  child: Row(
                                    children: [
                                      Icon(CustomIcons.pin, color: const LightColorTheme().white),
                                      const SizedBox(width: 8),
                                      BodyMediumBold(
                                          text: '${state.weather?.location?.name}, ${state.weather?.location?.country}',
                                          color: const LightColorTheme().white),
                                    ],
                                  ),
                                ),
                              ),
                              Stack(
                                alignment: Alignment.center,
                                children: [const BlurEllipse(), Image.asset(state.assetPath, height: 180, width: 180)],
                              ),
                              DisplayLargeBold(
                                  text: '${state.weather?.current?.tempC?.round()}\u00BA',
                                  color: const LightColorTheme().white),
                              BodyLarge(
                                  text: state.weather?.current?.condition?.text ?? '',
                                  color: const LightColorTheme().white),
                              const SizedBox(height: 8),
                              BodyLarge(
                                text:
                                    'Макс.: ${state.weather?.forecast?.forecastday?.single.day?.maxtempC?.round()}\u00BA'
                                    ' Мин: ${state.weather?.forecast?.forecastday?.single.day?.mintempC?.round()}\u00BA',
                                color: const LightColorTheme().white,
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(vertical: 24),
                                child: WeatherList(
                                  hour: state.weather?.forecast?.forecastday?.single.hour,
                                  date: state.weather?.location?.localtime?.substring(0, 10),
                                ),
                              ),
                              WindHumidityCard(
                                windSpeed: '${state.weather?.current?.windKph?.round()} км/ч',
                                windText: state.windDir,
                                humidityDegree: '${state.weather?.current?.humidity?.round()}%',
                                humidityText: state.humidity,
                              )
                            ],
                          ),
                        ),
            ),
          ),
        ),
      );
    });
  }
}
