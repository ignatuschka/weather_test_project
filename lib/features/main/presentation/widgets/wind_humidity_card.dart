import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:weather_test_project/core/theme/app_color_theme.dart';
import 'package:weather_test_project/features/auth/presentation/widgets/body_medium.dart';
import 'package:weather_test_project/features/auth/presentation/widgets/body_medium_bold.dart';

class WindHumidityCard extends StatelessWidget {
  final String windSpeed;
  final String windText;
  final String humidityDegree;
  final String humidityText;

  const WindHumidityCard({
    Key? key,
    required this.windSpeed,
    required this.windText,
    required this.humidityDegree,
    required this.humidityText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: const LightColorTheme().cardBackground,
      ),
      child: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              children: [
                SvgPicture.asset(
                  'assets/Wind.svg',
                  width: 24,
                  height: 24,
                ),
                const SizedBox(width: 8),
                SizedBox(
                  width: 56,
                  child: BodyMediumBold(
                    text: windSpeed,
                    color: const LightColorTheme().whiteOpacity,
                  ),
                ),
                const SizedBox(width: 24),
                BodyMedium(text: windText, color: const LightColorTheme().white),
              ],
            ),
            const SizedBox(height: 16),
            Row(
              children: [
                SvgPicture.asset(
                  'assets/Drop.svg',
                  width: 24,
                  height: 24,
                ),
                const SizedBox(width: 8),
                SizedBox(
                  width: 56,
                  child: BodyMediumBold(
                    text: humidityDegree,
                    color: const LightColorTheme().whiteOpacity,
                  ),
                ),
                const SizedBox(width: 24),
                BodyMedium(text: humidityText, color: const LightColorTheme().white),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
