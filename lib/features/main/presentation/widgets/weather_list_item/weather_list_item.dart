import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:weather_test_project/core/theme/app_color_theme.dart';
import 'package:weather_test_project/features/auth/presentation/widgets/body_large_bold.dart';
import 'package:weather_test_project/features/auth/presentation/widgets/body_medium.dart';
import 'package:weather_test_project/features/auth/presentation/widgets/custom_loading_indicator.dart';
import 'package:weather_test_project/features/main/presentation/widgets/weather_list_item/bloc/weather_list_item_bloc.dart';
import 'package:weather_test_project/features/main/presentation/widgets/weather_list_item/bloc/weather_list_item_event.dart';
import 'package:weather_test_project/features/main/presentation/widgets/weather_list_item/bloc/weather_list_item_state.dart';

class WeatherListItem extends StatelessWidget {
  const WeatherListItem({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<WeatherListItemBloc, WeatherListItemState>(
      builder: (context, state) {
        if (!state.isLoading) {
          return InkWell(
            onTap: () {
              context.read<WeatherListItemBloc>().add(ChosenEvent());
            },
            child: Container(
              decoration: state.isChosen
                  ? BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      border: Border.all(color: const LightColorTheme().white),
                      color: const LightColorTheme().listItemBackground)
                  : BoxDecoration(color: const LightColorTheme().transparent),
              child: Padding(
                padding: const EdgeInsets.all(16),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    BodyMedium(text: state.hourEntity?.time?.substring(11) ?? '', color: const LightColorTheme().white),
                    Padding(
                        padding: const EdgeInsets.symmetric(vertical: 16), child: SvgPicture.asset(state.assetPath)),
                    BodyLargeBold(
                        text: '${state.hourEntity?.tempC?.round()}\u00BA', color: const LightColorTheme().white)
                  ],
                ),
              ),
            ),
          );
        } else {
          return const CustomLoadingIndicator();
        }
      },
    );
  }
}
