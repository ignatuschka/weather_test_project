// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_test_project/core/globals.dart';

import 'package:weather_test_project/features/main/domain/entity/hour_entity.dart';
import 'package:weather_test_project/features/main/presentation/widgets/weather_list_item/bloc/weather_list_item_event.dart';
import 'package:weather_test_project/features/main/presentation/widgets/weather_list_item/bloc/weather_list_item_state.dart';

class WeatherListItemBloc extends Bloc<WeatherListItemEvent, WeatherListItemState> {
  final HourEntity? hourEntity;

  WeatherListItemBloc({
    required this.hourEntity,
  }) : super(WeatherListItemState.initial()) {
    on<WeatherListItemInitialEvent>(_initial);
    on<ChosenEvent>(_chosenEvent);
    add(WeatherListItemInitialEvent());
  }

  Future<void> _initial(WeatherListItemInitialEvent event, Emitter<WeatherListItemState> emit) async {
    emit(state.setIsLoadion(true));
    emit(state.setHourItem(hourEntity));
    emit(state.setAssetPath(smallAssetPath[hourEntity?.condition?.code ?? 1000] ?? 'assets/Sun.svg'));
    emit(state.setIsLoadion(false));
  }

  void _chosenEvent(ChosenEvent event, Emitter<WeatherListItemState> emit) {
    emit(state.setIsChosen(!state.isChosen));
  }
}
