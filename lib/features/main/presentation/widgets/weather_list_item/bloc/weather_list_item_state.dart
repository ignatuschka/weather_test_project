import 'package:built_value/built_value.dart';
import 'package:weather_test_project/features/main/domain/entity/hour_entity.dart';

part 'weather_list_item_state.g.dart';

abstract class WeatherListItemState implements Built<WeatherListItemState, WeatherListItemStateBuilder> {
  bool get isLoading;
  HourEntity? get hourEntity;
  bool get isChosen;
  String get assetPath;

  WeatherListItemState._();

  factory WeatherListItemState([void Function(WeatherListItemStateBuilder)? updates]) = _$WeatherListItemState;

  factory WeatherListItemState.initial() => WeatherListItemState((b) => b
    ..isLoading = true
    ..hourEntity = null
    ..isChosen = false
    ..assetPath = '');

  WeatherListItemState setIsLoadion(bool isLoading) => rebuild((b) => (b)..isLoading = isLoading);

  WeatherListItemState setHourItem(HourEntity? hourEntity) => rebuild((b) => (b)..hourEntity = hourEntity);

  WeatherListItemState setIsChosen(bool isChosen) => rebuild((b) => (b)..isChosen = isChosen);

  WeatherListItemState setAssetPath(String assetPath) => rebuild((b) => (b)..assetPath = assetPath);
}
