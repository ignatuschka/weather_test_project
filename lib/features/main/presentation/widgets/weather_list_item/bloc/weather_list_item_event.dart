abstract class WeatherListItemEvent {
  const WeatherListItemEvent();
}

class WeatherListItemInitialEvent extends WeatherListItemEvent {}

class ChosenEvent extends WeatherListItemEvent {}
