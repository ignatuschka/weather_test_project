import 'package:flutter/material.dart';
import 'package:weather_test_project/core/theme/app_color_theme.dart';

class BlurEllipse extends StatelessWidget {
  const BlurEllipse({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        boxShadow: [
          BoxShadow(
            blurRadius: 90,
            spreadRadius: 2,
            color: const LightColorTheme().purple,
          ),
        ],
      ),
      height: 150,
      width: 150,
    );
  }
}
