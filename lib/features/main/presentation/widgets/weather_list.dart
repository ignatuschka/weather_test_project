import 'package:flutter/material.dart';
import 'package:weather_test_project/core/string_extention.dart';

import 'package:weather_test_project/core/theme/app_color_theme.dart';
import 'package:weather_test_project/features/auth/presentation/widgets/body_large_bold.dart';
import 'package:weather_test_project/features/auth/presentation/widgets/body_medium.dart';
import 'package:weather_test_project/features/main/di/main_di_module.dart';
import 'package:weather_test_project/features/main/domain/entity/hour_entity.dart';

class WeatherList extends StatelessWidget {
  final List<HourEntity>? hour;
  final String? date;
  const WeatherList({
    Key? key,
    required this.hour,
    required this.date,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: const LightColorTheme().cardBackground,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(16),
            child: Row(
              children: [
                BodyLargeBold(
                  text: 'Cегодня',
                  color: const LightColorTheme().white,
                ),
                const Spacer(),
                BodyMedium(
                  text: date?.stringDateFormatting() ?? '',
                  color: const LightColorTheme().white,
                )
              ],
            ),
          ),
          const Divider(),
          Padding(
            padding: const EdgeInsets.all(16),
            child: SizedBox(
              height: 144,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemExtent: 74,
                itemCount: hour?.length,
                shrinkWrap: true,
                itemBuilder: (BuildContext context, int index) {
                  return WeatherListItemPage(
                    hourEntity: hour?[index],
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
