import 'package:weather_test_project/core/base_usecase.dart';
import 'package:weather_test_project/features/main/domain/entity/weather_entity.dart';
import 'package:weather_test_project/features/main/domain/repository/main_repository.dart';

class GetWeatherUsecase extends BaseUsecase<WeatherEntity, String> {
  final MainRepository repository;
  GetWeatherUsecase({
    required this.repository,
  });

  @override
 Future<WeatherEntity> call(String params) async {
    return await repository.getWeather(lonLat: params);
  }
}
