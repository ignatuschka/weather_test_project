import 'package:location/location.dart';
import 'package:weather_test_project/core/base_usecase.dart';
import 'package:weather_test_project/features/main/domain/repository/main_repository.dart';

class GetCurrentLocationUsecase extends BaseUsecase<LocationData, EmptyParams> {
  final MainRepository repository;
  GetCurrentLocationUsecase({
    required this.repository,
  });
  
  @override
  Future<LocationData> call(EmptyParams params) async {
    return await repository.getCurrentLocation();
  }
}
