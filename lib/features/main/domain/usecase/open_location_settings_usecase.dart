import 'package:weather_test_project/core/base_usecase.dart';
import 'package:weather_test_project/features/main/domain/repository/main_repository.dart';

class OpenLocationSettingsUsecase extends BaseUsecase<void, EmptyParams> {
  final MainRepository repository;
  OpenLocationSettingsUsecase({
    required this.repository,
  });
  
  @override
  Future<void> call(EmptyParams params) async {
    return await repository.openLocationSettings();
  }
}
