import 'package:weather_test_project/core/base_usecase.dart';
import 'package:weather_test_project/features/main/domain/entity/weather_entity.dart';
import 'package:weather_test_project/features/main/domain/repository/main_repository.dart';

class CreateOrUpdateWeatherUsecase extends BaseUsecase<void, WeatherEntity> {
  final MainRepository repository;
  CreateOrUpdateWeatherUsecase({
    required this.repository,
  });

  @override
 Future<void> call(WeatherEntity params) async {
    return await repository.createOrUpdateWeather(model: params);
  }
}
