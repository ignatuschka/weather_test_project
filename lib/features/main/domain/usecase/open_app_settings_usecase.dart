import 'package:weather_test_project/core/base_usecase.dart';
import 'package:weather_test_project/features/main/domain/repository/main_repository.dart';

class OpenAppSettingsUsecase extends BaseUsecase<void, EmptyParams> {
  final MainRepository repository;
  OpenAppSettingsUsecase({
    required this.repository,
  });
  
  @override
  Future<void> call(EmptyParams params) async {
    return await repository.openAppSettings();
  }
}
