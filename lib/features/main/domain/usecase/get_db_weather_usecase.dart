import 'package:weather_test_project/core/base_usecase.dart';
import 'package:weather_test_project/features/main/domain/entity/weather_entity.dart';
import 'package:weather_test_project/features/main/domain/repository/main_repository.dart';

class GetDbWeatherUsecase extends BaseUsecase<WeatherEntity, EmptyParams> {
  final MainRepository repository;
  GetDbWeatherUsecase({
    required this.repository,
  });

  @override
 Future<WeatherEntity> call(EmptyParams params) async {
    return await repository.getWeatherForecast();
  }
}
