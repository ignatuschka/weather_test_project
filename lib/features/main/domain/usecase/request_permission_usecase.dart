import 'package:weather_test_project/core/base_usecase.dart';
import 'package:weather_test_project/features/main/domain/repository/main_repository.dart';

class RequestPermissionUsecase extends BaseUsecase<bool, EmptyParams> {
  final MainRepository repository;
  RequestPermissionUsecase({
    required this.repository,
  });
  
  @override
  Future<bool> call(EmptyParams params) async {
    return await repository.requestPermission();
  }
}
