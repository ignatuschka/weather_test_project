import 'package:weather_test_project/features/main/data/models/forecast_model.dart';
import 'package:weather_test_project/features/main/domain/entity/forecast_day_entity.dart';

class ForecastEntity {
  List<ForecastDayEntity>? forecastday;
  ForecastEntity({
    required this.forecastday
  });

  ForecastModel toModel() => ForecastModel(
    forecastday: forecastday?.map((e) => e.toModel()).toList()
  );
}
