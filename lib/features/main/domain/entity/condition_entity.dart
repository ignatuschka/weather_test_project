import 'package:weather_test_project/features/main/data/models/condition_model.dart';

class ConditionEntity {
  String? text;
  String? icon;
  int? code;
  ConditionEntity({
    required this.text,
    required this.icon,
    required this.code,
  });

  ConditionModel toModel() => ConditionModel(
    text: text,
    icon: icon,
    code: code,
  );
}
