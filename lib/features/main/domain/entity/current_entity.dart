import 'package:weather_test_project/features/main/data/models/current_model.dart';
import 'package:weather_test_project/features/main/domain/entity/condition_entity.dart';

class CurrentEntity {
  int? lastUpdatedEpoch;
  String? lastUpdated;
  double? tempC;
  double? tempF;
  int? isDay;
  ConditionEntity? condition;
  double? windMph;
  double? windKph;
  double? windDegree;
  String? windDir;
  double? pressureMb;
  double? pressureIn;
  double? precipMm;
  double? precipIn;
  double? humidity;
  double? cloud;
  double? feelslikeC;
  double? feelslikeF;
  double? visKm;
  double? visMiles;
  double? uv;
  double? gustMph;
  double? gustKph;
  CurrentEntity({
    required this.lastUpdatedEpoch,
    required this.lastUpdated,
    required this.tempC,
    required this.tempF,
    required this.isDay,
    required this.condition,
    required this.windMph,
    required this.windKph,
    required this.windDegree,
    required this.windDir,
    required this.pressureMb,
    required this.pressureIn,
    required this.precipMm,
    required this.precipIn,
    required this.humidity,
    required this.cloud,
    required this.feelslikeC,
    required this.feelslikeF,
    required this.visKm,
    required this.visMiles,
    required this.uv,
    required this.gustMph,
    required this.gustKph,
  });

  CurrentModel toModel() => CurrentModel(
    lastUpdatedEpoch: lastUpdatedEpoch,
    lastUpdated: lastUpdated,
    tempC: tempC,
    tempF: tempF,
    isDay: isDay,
    condition: condition?.toModel(),
    windMph: windMph,
    windKph: windKph,
    windDegree: windDegree,
    windDir: windDir,
    pressureMb: pressureMb,
    pressureIn: pressureIn,
    precipMm: precipMm,
    precipIn: precipIn,
    humidity: humidity,
    cloud: cloud,
    feelslikeC: feelslikeC,
    feelslikeF: feelslikeF,
    visKm: visKm,
    visMiles: visMiles,
    uv: uv,
    gustMph: gustMph,
    gustKph: gustKph,
  );
}
