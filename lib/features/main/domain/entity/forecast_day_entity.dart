import 'package:weather_test_project/features/main/data/models/forecast_day_model.dart';
import 'package:weather_test_project/features/main/domain/entity/astro_entity.dart';
import 'package:weather_test_project/features/main/domain/entity/day_entity.dart';
import 'package:weather_test_project/features/main/domain/entity/hour_entity.dart';

class ForecastDayEntity {
  String? date;
  int? dateEpoch;
  DayEntity? day;
  AstroEntity? astro;
  List<HourEntity>? hour;
  ForecastDayEntity({
    required this.date,
    required this.dateEpoch,
    required this.day,
    required this.astro,
    required this.hour,
  });

  ForecastDayModel toModel() => ForecastDayModel(
    date: date,
    dateEpoch: dateEpoch,
    day: day?.toModel(),
    astro: astro?.toModel(),
    hour: hour?.map((e) => e.toModel()).toList(),
  );
}
