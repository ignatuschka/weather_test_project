import 'package:weather_test_project/features/main/data/models/location_model.dart';

class LocationEntity {
  String? name;
  String? region;
  String? country;
  double? lat;
  double? lon;
  String? tzId;
  int? localtimeEpoch;
  String? localtime;
  LocationEntity({
    required this.name,
    required this.region,
    required this.country,
    required this.lat,
    required this.lon,
    required this.tzId,
    required this.localtimeEpoch,
    required this.localtime,
  });

  LocationModel toModel() => LocationModel(
    name: name,
    region: region,
    country: country,
    lat: lat,
    lon: lon,
    tzId: tzId,
    localtimeEpoch: localtimeEpoch,
    localtime: localtime,
  );
}
