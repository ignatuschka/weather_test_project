import 'package:weather_test_project/features/main/data/models/astro_model.dart';

class AstroEntity {
  String? sunrise;
  String? sunset;
  String? moonrise;
  String? moonset;
  String? moonPhase;
  int? moonIllumination;
  int? isMoonUp;
  int? isSunUp;
  AstroEntity({
    required this.sunrise,
    required this.sunset,
    required this.moonrise,
    required this.moonset,
    required this.moonPhase,
    required this.moonIllumination,
    required this.isMoonUp,
    required this.isSunUp,
  });

  AstroModel toModel() => AstroModel(
    sunrise: sunrise,
    sunset: sunset,
    moonrise: moonrise,
    moonset: moonset,
    moonPhase: moonPhase,
    moonIllumination: moonIllumination,
    isMoonUp: isMoonUp,
    isSunUp: isSunUp,
  );
}
