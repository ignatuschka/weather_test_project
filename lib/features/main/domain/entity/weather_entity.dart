import 'package:weather_test_project/features/main/data/models/weather_model.dart';
import 'package:weather_test_project/features/main/domain/entity/current_entity.dart';
import 'package:weather_test_project/features/main/domain/entity/forecast_entity.dart';
import 'package:weather_test_project/features/main/domain/entity/location_entity.dart';

class WeatherEntity {
  LocationEntity? location;
  CurrentEntity? current;
  ForecastEntity? forecast;
  WeatherEntity({
    required this.location,
    required this.current,
    required this.forecast,
  });

  WeatherModel toModel() => WeatherModel(
    location: location?.toModel(),
    current: current?.toModel(),
    forecast: forecast?.toModel(),
  );
}
