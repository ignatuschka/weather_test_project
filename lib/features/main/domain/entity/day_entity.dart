import 'package:weather_test_project/features/main/data/models/day_model.dart';
import 'package:weather_test_project/features/main/domain/entity/condition_entity.dart';

class DayEntity {
  double? maxtempC;
  double? maxtempF;
  double? mintempC;
  double? mintempF;
  double? avgtempC;
  double? avgtempF;
  double? maxwindMph;
  double? maxwindKph;
  double? totalprecipMm;
  double? totalprecipIn;
  double? totalsnowCm;
  double? avgvisKm;
  double? avgvisMiles;
  double? avghumidity;
  int? dailyWillItRain;
  double? dailyChanceOfRain;
  int? dailyWillItSnow;
  double? dailyChanceOfSnow;
  ConditionEntity? condition;
  double? uv;
  DayEntity({
    required this.maxtempC,
    required this.maxtempF,
    required this.mintempC,
    required this.mintempF,
    required this.avgtempC,
    required this.avgtempF,
    required this.maxwindMph,
    required this.maxwindKph,
    required this.totalprecipMm,
    required this.totalprecipIn,
    required this.totalsnowCm,
    required this.avgvisKm,
    required this.avgvisMiles,
    required this.avghumidity,
    required this.dailyWillItRain,
    required this.dailyChanceOfRain,
    required this.dailyWillItSnow,
    required this.dailyChanceOfSnow,
    required this.condition,
    required this.uv,
  });

  DayModel toModel() => DayModel(
    maxtempC: maxtempC,
    maxtempF: maxtempF,
    mintempC: mintempC,
    mintempF: mintempF,
    avgtempC: avgtempC,
    avgtempF: avgtempF,
    maxwindMph: maxwindMph,
    maxwindKph: maxwindKph,
    totalprecipMm: totalprecipMm,
    totalprecipIn: totalprecipIn,
    totalsnowCm: totalsnowCm,
    avgvisKm: avgvisKm,
    avgvisMiles: avgvisMiles,
    avghumidity: avghumidity,
    dailyWillItRain: dailyWillItRain,
    dailyChanceOfRain: dailyChanceOfRain,
    dailyWillItSnow: dailyWillItSnow,
    dailyChanceOfSnow: dailyChanceOfSnow,
    condition: condition?.toModel(),
    uv: uv,
  );
}
