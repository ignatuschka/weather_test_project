import 'package:location/location.dart';
import 'package:weather_test_project/features/main/domain/entity/weather_entity.dart';

abstract class MainRepository {
  Future<WeatherEntity> getWeather({required String lonLat});
  Future<bool> requestPermission();
  Future<LocationData> getCurrentLocation();
  Future<void> createOrUpdateWeather({required WeatherEntity model});
  Future<WeatherEntity> getWeatherForecast();
  Future<bool> checkConnection();
  Future<void> openAppSettings();
  Future<void> openLocationSettings();
}