import 'package:auto_route/auto_route.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';
import 'package:weather_test_project/core/core_di_module.dart';
import 'package:weather_test_project/core/globals.dart';
import 'package:weather_test_project/core/theme/app_color_theme.dart';
import 'package:weather_test_project/firebase_options.dart';
import 'package:weather_test_project/navigation/app_router.dart';

GetIt getIt = GetIt.I;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: const LightColorTheme().transparent,
    systemNavigationBarColor: const LightColorTheme().transparent,
    systemNavigationBarDividerColor: const LightColorTheme().transparent,
  ));
  await CoreDIModule().updateInjections(getIt);
  await Future.delayed(const Duration(seconds: 2));
  runApp(MaterialApp.router(
    theme: ThemeData(useMaterial3: true, scaffoldBackgroundColor: const LightColorTheme().white),
    scaffoldMessengerKey: scaffoldKey,
    routerDelegate: AutoRouterDelegate(getIt<AppRouter>()),
    routeInformationParser: getIt<AppRouter>().defaultRouteParser(),
  ));
}
