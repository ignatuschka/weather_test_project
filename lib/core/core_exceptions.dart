import 'package:dio/dio.dart';
import 'package:firebase_auth/firebase_auth.dart';

abstract class CoreException {
  final String messageExseption;
  const CoreException(this.messageExseption);
}

abstract class CoreFireException extends FirebaseAuthException implements CoreException {
  @override
  final String messageExseption;
  CoreFireException(this.messageExseption, {required super.code});
}

abstract class CoreDioException extends DioError implements CoreException {
  @override
  final String messageExseption;
  CoreDioException(this.messageExseption, {required super.requestOptions});
}

class RequestCancelledException extends CoreDioException {
  RequestCancelledException({required super.requestOptions}) : super('Запрос был отменен. Попробуйте еще раз');
}

class ConnectTimeoutException extends CoreDioException {
  ConnectTimeoutException({required super.requestOptions}) : super('Ошибка соединения. Попробуйте еще раз');
}

class ReceiveTimeoutException extends CoreDioException {
  ReceiveTimeoutException({required super.requestOptions}) : super('Ошибка получения ответа. Попробуйте еще раз');
}

class SendTimeoutException extends CoreDioException {
  SendTimeoutException({required super.requestOptions}) : super('Ошибка отправки запроса. Попробуйте еще раз');
}

class UnknownServerException extends CoreDioException {
  final int? statusCode;
  UnknownServerException({required this.statusCode, required super.requestOptions})
      : super('Код сетевой ошибки: $statusCode. Попробуйте позднее');
}

class DefaultNetworkException extends CoreDioException {
  DefaultNetworkException({required super.requestOptions}) : super('Что-то не так. Попробуйте позднее');
}

class InvalidEmailException extends CoreFireException {
  InvalidEmailException({required super.code}) : super('Aдрес электронной почты недействителен');
}

class DisabledUserException extends CoreFireException {
  DisabledUserException({required super.code}) : super('Пользователь отключен');
}

class UserNotFoundedException extends CoreFireException {
  UserNotFoundedException({required super.code}) : super('Пользователь не найден');
}

class WrongPasswordException extends CoreFireException {
  WrongPasswordException({required super.code}) : super('Неправильный пароль');
}

class InvalidCredentialException extends CoreFireException {
  InvalidCredentialException({required super.code}) : super('Учетные данные неверны или срок их действия истек');
}

class NetworkFirebaseException extends CoreFireException {
  NetworkFirebaseException({required super.code})
      : super('Произошла сетевая ошибка (например, тайм-аут, прерванное соединение или недоступный хост)');
}

class DefaultFireException extends CoreFireException {
  DefaultFireException({required super.code}) : super('Что-то пошло не так. Код ошибки: $code');
}

class LocatioServiceNotEnabledException extends CoreException {
  const LocatioServiceNotEnabledException() : super('Служба местоположения не включена');
}

class PermissionAccessException extends CoreException {
  const PermissionAccessException() : super('Приложению необходим доступ к данным о местоположении устройства');
}
