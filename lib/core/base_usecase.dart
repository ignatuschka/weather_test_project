import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:weather_test_project/core/core_exceptions.dart';
import 'package:weather_test_project/core/globals.dart';

abstract class BaseUsecase<Type, Params> {
  Future<Type> call(Params params);
}

class EmptyParams {}

Future<void> processUseCase<Type>(
  Future<Type> Function() computation, {
  Future<void> Function(Type result)? onSucess,
  Future<void> Function(String exception)? onError,
}) async {
  try {
    final result = await computation();
    if (onSucess != null) {
      await onSucess(result);
    }
  } on CoreException catch (e) {
    if (onError != null) {
      await onError(e.messageExseption);
    } else {
      await showCustomSnackBar(event: e.messageExseption);
    }
  }
}

Future<Type> checkFirebaseErrors<Type>(Future<Type> Function() computation) async {
  try {
    final result = await computation();
    return result;
  } on FirebaseAuthException catch (e) {
    switch (e.code) {
      case 'invalid-email':
        throw InvalidEmailException(code: e.code);
      case 'user-disabled':
        throw DisabledUserException(code: e.code);
      case 'user-not-found':
        throw UserNotFoundedException(code: e.code);
      case 'wrong-password':
        throw WrongPasswordException(code: e.code);
      case 'invalid-credential':
        throw InvalidCredentialException(code: e.code);
      case 'network-request-failed':
        throw NetworkFirebaseException(code: e.code);
      default:
        throw DefaultFireException(code: e.code);
    }
  }
}
