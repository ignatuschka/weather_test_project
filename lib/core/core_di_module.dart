import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:weather_test_project/features/auth/di/auth_di_module.dart';
import 'package:weather_test_project/features/main/di/main_di_module.dart';
import 'package:weather_test_project/navigation/app_router.dart';
import 'package:weather_test_project/navigation/auth_guard.dart';
import 'package:weather_test_project/navigation/auth_navigation.dart';

class CoreDIModule {
  Future<void> updateInjections(GetIt instance) async {
    AuthDIModule().updateInjections(instance);
    MainDIModule().updateInjections(instance);
    instance.registerSingleton<SharedPreferences>(await SharedPreferences.getInstance());
    instance.registerSingleton<AppRouter>(AppRouter(authGuard: AuthGuard(instance: instance.get())));
    instance.registerSingleton<AuthNavigator>(AuthNavigatorImpl());
  }
}
