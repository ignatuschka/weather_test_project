import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:weather_test_project/core/theme/app_color_theme.dart';
import 'package:weather_test_project/features/auth/presentation/widgets/body_small.dart';
import 'package:weather_test_project/features/auth/resources/auth_text.dart';

final GlobalKey<ScaffoldMessengerState> scaffoldKey = GlobalKey<ScaffoldMessengerState>();

Future<void> showCustomSnackBar({
  required String event,
  bool? needAction,
  bool? isError,
  String? label,
  Future<void> Function()? onTap,
}) async {
  scaffoldKey.currentState?.clearSnackBars();
  await Future.microtask(
    () => scaffoldKey.currentState?.showSnackBar(
      SnackBar(
        shape: OutlineInputBorder(
          borderRadius: BorderRadius.circular(24),
          borderSide: BorderSide(
            color: (isError ?? true) ? const LightColorTheme().red : const LightColorTheme().purple,
          ),
        ),
        actionOverflowThreshold: 1,
        elevation: 0,
        behavior: SnackBarBehavior.floating,
        margin: const EdgeInsets.symmetric(vertical: 8, horizontal: 24),
        backgroundColor: const LightColorTheme().cardBackground,
        content: Center(
          child: BodySmall(
            text: event,
            color: (isError ?? true) ? const LightColorTheme().red : const LightColorTheme().purple,
          ),
        ),
        action: (needAction ?? false)
            ? SnackBarAction(
                textColor: const LightColorTheme().red,
                label: label ?? AuthText.copy,
                onPressed: () async {
                  if (onTap != null) {
                    await onTap();
                  } else {
                    Clipboard.setData(ClipboardData(text: event));
                  }
                },
              )
            : null,
      ),
    ),
  );
}

const apiKey = '356fb936c61647fdbbd04003240203';

const lang = 'ru';

const windDir = {
  'S': 'Ветер южный',
  'W': 'Ветер западный',
  'N': 'Ветер северный',
  'E': 'Ветер восточный',
  'SW': 'Ветер юго-западный',
  'SSW': 'Ветер юго-западный',
  'WSW': 'Ветер юго-западный',
  'SE': 'Ветер юго-восточный',
  'SSE': 'Ветер юго-восточный',
  'ESE': 'Ветер юго-восточный',
  'NW': 'Ветер северо-западный',
  'NNW': 'Ветер северо-западный',
  'WNW': 'Ветер северо-западный',
  'NE': 'Ветер северо-восточный',
  'NNE': 'Ветер северо-восточный',
  'ENE': 'Ветер северо-восточный',
};

const bigAssetPath = {
  1063: 'assets/BigCloudHeavyRain.png',
  1172: 'assets/BigCloudHeavyRain.png',
  1192: 'assets/BigCloudHeavyRain.png',
  1195: 'assets/BigCloudHeavyRain.png',
  1201: 'assets/BigCloudHeavyRain.png',
  1243: 'assets/BigCloudHeavyRain.png',
  1246: 'assets/BigCloudHeavyRain.png',
  1249: 'assets/BigCloudHeavyRain.png',
  1252: 'assets/BigCloudHeavyRain.png',
  1087: 'assets/BigCloudLightning.png',
  1273: 'assets/BigCloudLightning.png',
  1276: 'assets/BigCloudLightning.png',
  1279: 'assets/BigCloudLightning.png',
  1282: 'assets/BigCloudLightning.png',
  1006: 'assets/BigCloudRain.png',
  1009: 'assets/BigCloudRain.png',
  1030: 'assets/BigCloudRain.png',
  1072: 'assets/BigCloudRain.png',
  1135: 'assets/BigCloudRain.png',
  1147: 'assets/BigCloudRain.png',
  1150: 'assets/BigCloudRain.png',
  1153: 'assets/BigCloudRain.png',
  1168: 'assets/BigCloudRain.png',
  1180: 'assets/BigCloudRain.png',
  1183: 'assets/BigCloudRain.png',
  1186: 'assets/BigCloudRain.png',
  1189: 'assets/BigCloudRain.png',
  1198: 'assets/BigCloudRain.png',
  1240: 'assets/BigCloudRain.png',
  1003: 'assets/BigCloudSunRain.png',
  1000: 'assets/BigSun.png',
  1066: 'assets/BigCloudSnow.png',
  1069: 'assets/BigCloudSnow.png',
  1114: 'assets/BigCloudSnow.png',
  1117: 'assets/BigCloudSnow.png',
  1204: 'assets/BigCloudSnow.png',
  1207: 'assets/BigCloudSnow.png',
  1210: 'assets/BigCloudSnow.png',
  1213: 'assets/BigCloudSnow.png',
  1216: 'assets/BigCloudSnow.png',
  1219: 'assets/BigCloudSnow.png',
  1222: 'assets/BigCloudSnow.png',
  1225: 'assets/BigCloudSnow.png',
  1237: 'assets/BigCloudSnow.png',
  1255: 'assets/BigCloudSnow.png',
  1258: 'assets/BigCloudSnow.png',
  1261: 'assets/BigCloudSnow.png',
  1264: 'assets/BigCloudSnow.png',
};

const smallAssetPath = {
  1087: 'assets/CloudLightning.svg',
  1273: 'assets/CloudLightning.svg',
  1276: 'assets/CloudLightning.svg',
  1279: 'assets/CloudLightning.svg',
  1282: 'assets/CloudLightning.svg',
  1009: 'assets/CloudRain.svg',
  1030: 'assets/CloudRain.svg',
  1063: 'assets/CloudRain.svg',
  1072: 'assets/CloudRain.svg',
  1135: 'assets/CloudRain.svg',
  1147: 'assets/CloudRain.svg',
  1150: 'assets/CloudRain.svg',
  1153: 'assets/CloudRain.svg',
  1168: 'assets/CloudRain.svg',
  1172: 'assets/CloudRain.svg',
  1180: 'assets/CloudRain.svg',
  1183: 'assets/CloudRain.svg',
  1189: 'assets/CloudRain.svg',
  1192: 'assets/CloudRain.svg',
  1195: 'assets/CloudRain.svg',
  1198: 'assets/CloudRain.svg',
  1201: 'assets/CloudRain.svg',
  1240: 'assets/CloudRain.svg',
  1243: 'assets/CloudRain.svg',
  1246: 'assets/CloudRain.svg',
  1249: 'assets/CloudRain.svg',
  1252: 'assets/CloudRain.svg',
  1255: 'assets/CloudRain.svg',
  1258: 'assets/CloudRain.svg',
  1066: 'assets/CloudSnow.svg',
  1069: 'assets/CloudSnow.svg',
  1114: 'assets/CloudSnow.svg',
  1117: 'assets/CloudSnow.svg',
  1204: 'assets/CloudSnow.svg',
  1207: 'assets/CloudSnow.svg',
  1210: 'assets/CloudSnow.svg',
  1213: 'assets/CloudSnow.svg',
  1216: 'assets/CloudSnow.svg',
  1219: 'assets/CloudSnow.svg',
  1222: 'assets/CloudSnow.svg',
  1225: 'assets/CloudSnow.svg',
  1237: 'assets/CloudSnow.svg',
  1261: 'assets/CloudSnow.svg',
  1264: 'assets/CloudSnow.svg',
  1003: 'assets/CloudSun.svg',
  1006: 'assets/CloudSun.svg',
  1000: 'assets/Sun.svg',
};
