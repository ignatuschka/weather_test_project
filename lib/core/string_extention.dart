import 'package:intl/date_symbol_data_local.dart';

extension DateTimeStringExtensions on String{
  DateTime isoToDateTime(){
    initializeDateFormatting("ru_RU", null);
    return DateTime.parse(this);
  }

  String stringDateFormatting() {
    return split('-').reversed.join('.');
  }
}
