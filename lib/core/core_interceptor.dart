import 'package:dio/dio.dart';
import 'package:weather_test_project/core/core_exceptions.dart';

class CoreInterceptor extends Interceptor {
  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    switch (err.type) {
      case DioErrorType.cancel:
        throw RequestCancelledException(requestOptions: err.requestOptions);
      case DioErrorType.connectTimeout:
        throw ConnectTimeoutException(requestOptions: err.requestOptions);
      case DioErrorType.receiveTimeout:
        throw ReceiveTimeoutException(requestOptions: err.requestOptions);
      case DioErrorType.sendTimeout:
        throw SendTimeoutException(requestOptions: err.requestOptions);
      case DioErrorType.response:
        throw UnknownServerException(statusCode: err.response?.statusCode, requestOptions: err.requestOptions);
      case DioErrorType.other:
        throw DefaultNetworkException(requestOptions: err.requestOptions);
    }
  }
}
