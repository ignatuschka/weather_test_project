import 'package:flutter/widgets.dart';

class CustomIcons {
  CustomIcons._();

  static const _kFontFam = 'CustomIcons';
  static const String? _kFontPkg = null;

  static const IconData cloudsnow = IconData(0xe800, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData eye = IconData(0xe801, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData cloudmoon = IconData(0xe802, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData cloudlightning = IconData(0xe803, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData drop = IconData(0xe804, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData wind = IconData(0xe805, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData cloudsun = IconData(0xe806, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData sun = IconData(0xe807, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData pin = IconData(0xe808, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData eyeOff2 = IconData(0xe809, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData cloudrain = IconData(0xe80a, fontFamily: _kFontFam, fontPackage: _kFontPkg);
}