import 'package:flutter/material.dart';
import 'package:weather_test_project/core/theme/app_palette.dart';

abstract class AppColorTheme {
  Brightness get brightness;

  Color get blue;

  Color get white;

  Color get black;

  Color get greyMedium;

  Color get red;

  Color get greyBorder;

  Color get cardBackground;

  Color get listItemBackground;

  Color get transparent;

  Color get whiteOpacity;

  Color get purple;
}

class LightColorTheme implements AppColorTheme {
  @override
  Brightness get brightness => Brightness.light;

  @override
  Color get blue => AppPallete.blue;

  @override
  Color get white => AppPallete.white;

  @override
  Color get black => AppPallete.black;

  @override
  Color get greyMedium => AppPallete.greyMedium;

  @override
  Color get red => AppPallete.red;

  @override
  Color get greyBorder => AppPallete.greyBorder;

  @override
  Color get cardBackground => AppPallete.cardBackground;

  @override
  Color get listItemBackground => AppPallete.listItemBackground;

  @override
  Color get transparent => AppPallete.transparent;

  @override
  Color get whiteOpacity => AppPallete.whiteOpacity;

  @override
  Color get purple => AppPallete.purple;

  const LightColorTheme() : super();
}
