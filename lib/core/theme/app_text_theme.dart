import 'package:flutter/material.dart';

abstract class AppTextTheme {
  TextStyle get heading1;

  TextStyle get heading2;

  TextStyle get body1;

  TextStyle get body2;

  TextStyle get body3;

  TextStyle get body1Medium;

  TextStyle get body2Medium;

  TextStyle get body3Medium;
}

class BaseTextTheme implements AppTextTheme {
  @override
  TextStyle get heading1 => const TextStyle(
        fontFamily: 'Ubuntu',
        fontWeight: FontWeight.w500,
        fontStyle: FontStyle.normal,
        fontSize: 32,
        height: 36,
      );

  @override
  TextStyle get heading2 => heading1.copyWith(fontSize: 22, height: 28);

  @override
  TextStyle get body1 => const TextStyle(
        fontFamily: 'Roboto',
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
        fontSize: 17,
        height: 24,
      );

  @override
  TextStyle get body2 => body1.copyWith(fontSize: 15, height: 22);
  
  @override
  TextStyle get body3 => body1.copyWith(fontSize: 13, height: 18);

  @override
  TextStyle get body1Medium => body1.copyWith(fontWeight: FontWeight.w500);
  
  @override
  TextStyle get body2Medium => body1.copyWith(fontWeight: FontWeight.w500);

  @override
  TextStyle get body3Medium => body1.copyWith(fontWeight: FontWeight.w500);

  const BaseTextTheme() : super();
}
