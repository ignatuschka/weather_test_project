import 'package:flutter/material.dart';

class AppPallete {
  static const Color greyMedium = Color(0xFF8799A5);

  static const Color white = Color(0xFFFFFFFF);

  static const Color black = Color(0xFF2B2D33);

  static const Color blue = Color(0xFF0700FF);

  static const Color red = Color(0xFFFF1E00);

  static const Color greyBorder = Color(0xFFE4E6EC);

  static const Color cardBackground = Color(0x33FFFFFF);

  static const Color listItemBackground = Color(0x66FFFFFF);

  static const Color whiteOpacity = Color(0x4DFFFFFF);

  static const Color transparent = Colors.transparent;

  static const Color purple = Color(0xFFBD87FF);
}