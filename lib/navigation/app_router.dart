import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:weather_test_project/features/auth/di/auth_di_module.dart';
import 'package:weather_test_project/features/main/di/main_di_module.dart';
import 'package:weather_test_project/navigation/auth_guard.dart';

part 'app_router.gr.dart';

const rootAuth = 'RootAuth';
const signInPage = 'signInPage';
const mainPage = 'mainPage';

@MaterialAutoRouter(
  replaceInRouteName: 'Page,Route',
  routes: [
    AutoRoute(
      path: mainPage,
      initial: true,
      page: MainPage,
      guards: [AuthGuard],
    ),
    AutoRoute(
      path: signInPage,
      page: SignInPage,
    ),
  ],
)
class AppRouter extends _$AppRouter {
  AppRouter({required super.authGuard});
}
