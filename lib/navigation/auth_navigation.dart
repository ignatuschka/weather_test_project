import 'package:get_it/get_it.dart';
import 'package:weather_test_project/navigation/app_router.dart';

GetIt getIt = GetIt.instance;

abstract class AuthNavigator {
  Future<void> navigateToMainScreen();

  void navigateBack();
}

class AuthNavigatorImpl implements AuthNavigator {
  static final AppRouter _ar = getIt<AppRouter>();
  
  @override
  void navigateBack() {
    _ar.pop();
  }
  
  @override
  Future<void> navigateToMainScreen() async {
    _ar.push(const MainRoute());
  }
}
