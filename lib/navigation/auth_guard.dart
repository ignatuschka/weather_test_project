import 'package:auto_route/auto_route.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:weather_test_project/navigation/app_router.dart';

GetIt getIt = GetIt.I;

class AuthGuard extends AutoRouteGuard {
  final SharedPreferences instance;
  AuthGuard({
    required this.instance,
  });
  @override
  void onNavigation(NavigationResolver resolver, StackRouter router) {
    if (instance.containsKey('uid')) {
      resolver.next(true);
    } else {
      router.push(const SignInRoute());
    }
  }
}
